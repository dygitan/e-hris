A web based application used for tracking & managing the end to end process for a company's recruitment requirements.

This application covers (includes) the following functionalities:

- Creation of request for new resource/s and approval of such requests
- Tracking of job vacancies
- Applicant's progress (including interview progress comments, etc.)
- Reports and dashboards for admin usage

Technologies used for this project are:

- Java
- Spring
- Hibernate
- Apache Tiles
- MySQL
- Maven
- Bootstrap
- JQuery

Demo application can be found: http://portfolio-ats.herokuapp.com

Login credentials: admin:123456
