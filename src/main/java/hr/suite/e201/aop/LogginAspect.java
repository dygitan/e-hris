package hr.suite.e201.aop;

import hr.suite.e201.model.BaseModel;
import hr.suite.e201.model.SecUser;
import hr.suite.e201.utility.SecurityUtility;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
@Configuration
@Aspect
public class LogginAspect {

    private static final Logger LOGGER = Logger.getLogger(LogginAspect.class);

    @Before("execution(* hr.suite.e201.dao.impl.*.save*(..))")
    public void logBefore(JoinPoint joinPoint) {
        LOGGER.debug("Triggered saveOrUpdate method ...");

        BaseModel base = (BaseModel) joinPoint.getArgs()[0];

        if (base.getCreatedBy() == null || StringUtils.isEmpty(base.getCreatedBy().getUsername())) {
            base.setCreatedBy(new SecUser(SecurityUtility.getCurrentUsername()));
            base.setCreatedDate(new Date());
        } else {
            base.setUpdatedBy(new SecUser(SecurityUtility.getCurrentUsername()));
            base.setUpdatedDate(new Date());
        }
    }
}
