package hr.suite.e201.bean.employee;

import hr.suite.e201.model.employee.Employee;

/**
 *
 * @author Patrick Tan
 * @since Dec 22, 2014
 */
public class EmployeeRequirementSummary {

    private Employee employee;
    private int requirementCounter;
    private int submittedCounter;
    private int notApplicableCounter;

    /**
     * @return the employee
     */
    public Employee getEmployee() {
        return employee;
    }

    /**
     * @param employee the employee to set
     */
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    /**
     * @return the requirementCounter
     */
    public int getRequirementCounter() {
        return requirementCounter;
    }

    /**
     * @param requirementCounter the requirementCounter to set
     */
    public void setRequirementCounter(int requirementCounter) {
        this.requirementCounter = requirementCounter;
    }

    /**
     * @return the submittedCounter
     */
    public int getSubmittedCounter() {
        return submittedCounter;
    }

    /**
     * @param submittedCounter the submittedCounter to set
     */
    public void setSubmittedCounter(int submittedCounter) {
        this.submittedCounter = submittedCounter;
    }

    /**
     * @return the notApplicableCounter
     */
    public int getNotApplicableCounter() {
        return notApplicableCounter;
    }

    /**
     * @param notApplicableCounter the notApplicableCounter to set
     */
    public void setNotApplicableCounter(int notApplicableCounter) {
        this.notApplicableCounter = notApplicableCounter;
    }

}
