package hr.suite.e201.bean.employee.upload;

import java.util.List;

/**
 *
 * @author Patrick Tan
 * @since Dec 29, 2014
 */
public class LegalFormBean {

    private List<LegalUploadBean> legalUploadBeans;

    /**
     * @return the legalUploadBeans
     */
    public List<LegalUploadBean> getLegalUploadBeans() {
        return legalUploadBeans;
    }

    /**
     * @param legalUploadBeans the legalUploadBeans to set
     */
    public void setLegalUploadBeans(List<LegalUploadBean> legalUploadBeans) {
        this.legalUploadBeans = legalUploadBeans;
    }
}
