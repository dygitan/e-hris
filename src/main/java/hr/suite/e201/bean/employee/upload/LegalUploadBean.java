package hr.suite.e201.bean.employee.upload;

import hr.suite.e201.model.employee.EmployeeLegal;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Patrick Tan
 * @since Dec 29, 2014
 */
public class LegalUploadBean {

    private MultipartFile attachment;
    private EmployeeLegal legal;

    /**
     * @return the attachment
     */
    public MultipartFile getAttachment() {
        return attachment;
    }

    /**
     * @param attachment the attachment to set
     */
    public void setAttachment(MultipartFile attachment) {
        this.attachment = attachment;
    }

    /**
     * @return the legal
     */
    public EmployeeLegal getLegal() {
        return legal;
    }

    /**
     * @param legal the legal to set
     */
    public void setLegal(EmployeeLegal legal) {
        this.legal = legal;
    }
}
