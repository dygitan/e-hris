package hr.suite.e201.bean.employee.upload;

import hr.suite.e201.model.employee.EmployeeRequirement;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Patrick Tan
 * @since Dec 22, 2014
 */
public class RequirementUploadBean {

    private MultipartFile attachment;
    private EmployeeRequirement requirement;

    /**
     * @return the attachment
     */
    public MultipartFile getAttachment() {
        return attachment;
    }

    /**
     * @param attachment the attachment to set
     */
    public void setAttachment(MultipartFile attachment) {
        this.attachment = attachment;
    }

    /**
     * @return the requirement
     */
    public EmployeeRequirement getRequirement() {
        return requirement;
    }

    /**
     * @param requirement the requirement to set
     */
    public void setRequirement(EmployeeRequirement requirement) {
        this.requirement = requirement;
    }

    
}
