package hr.suite.e201.builder;

import hr.suite.e201.model.employee.Employee;
import hr.suite.e201.model.employee.EmployeeMovement;
import hr.suite.e201.validator.employee.EmployeeValidator;
import hr.suite.e201.validator.employee.MovementValidator;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 *
 * @author Patrick Tan
 * @since Jan 3, 2015
 */
@Component
public class ValidatorBuilder {

    private static final Logger LOGGER = Logger.getLogger(ValidatorBuilder.class);
    private static final Map<Class, Validator> VALIDATOR_MAP = new HashMap<>();
    @Autowired
    private LocalValidatorFactoryBean defaultValidator;

    public Validator getValidator(Object target) {
        Validator validator = null;

        if (!VALIDATOR_MAP.containsKey(target.getClass())) {
            if (target instanceof Employee) {
                validator = new EmployeeValidator(defaultValidator);

            } else if (target instanceof EmployeeMovement) {
                validator = new MovementValidator(defaultValidator);
            }

            if (validator != null) {
                VALIDATOR_MAP.put(target.getClass(), validator);
            }
        } else {
            validator = VALIDATOR_MAP.get(target.getClass());
        }

        LOGGER.debug("Clazz [" + target.getClass() + "] validator: [" + validator + "]");

        return validator;
    }
}
