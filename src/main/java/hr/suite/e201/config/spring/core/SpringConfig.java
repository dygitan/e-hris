package hr.suite.e201.config.spring.core;

import com.mchange.v2.c3p0.DriverManagerDataSource;
import hr.suite.e201.config.spring.security.SecurityConfig;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
@EnableWebMvc
@Configuration
@ComponentScan({"hr.suite.e201.*"})
@EnableTransactionManagement
@EnableAspectJAutoProxy
@Import({SecurityConfig.class})
//@PropertySource("/WEB-INF/properties/application.properties")
public class SpringConfig {

    private static final Logger LOGGER = Logger.getLogger(SpringConfig.class);

    @Bean(name = "sessionFactory")
    public SessionFactory sessionFactory() {
        LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource());
        builder.scanPackages("hr.suite.e201.model").addProperties(getHibernateProperties());
        return builder.buildSessionFactory();
    }

    private Properties getHibernateProperties() {
        Properties prop = new Properties();

        prop.put("hibernate.format_sql", "true");
        prop.put("hibernate.show_sql", "false");
        prop.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");

//        prop.put("hibernate.cache.provider_class", "org.hibernate.cache.NoCacheProvider");
//        prop.put("hibernate.search.lucene_version", "LUCENE_47");
//        prop.put("hibernate.search.default.directory_provider", "org.hibernate.search.store.impl.FSDirectoryProvider");
//        prop.put("hibernate.search.default.indexBase", "/var/lucene/indexes");

        return prop;
    }

    @Bean(name = "dataSource")
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClass("com.mysql.jdbc.Driver");

//        ds.setJdbcUrl("jdbc:mysql://localhost:3306/e_201_db");
//        ds.setUser("root");
//        ds.setPassword("12345");

        ds.setJdbcUrl("jdbc:mysql://127.9.55.2:3306/e_201_db");
        ds.setUser("adminVZXDELd");
        ds.setPassword("L61zEdKhPfqR");
        
        return ds;
    }

    @Bean
    public HibernateTransactionManager txManager() {
        return new HibernateTransactionManager(sessionFactory());
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        return multipartResolver;
    }

    @Bean
    public TilesViewResolver viewResolver() {
        TilesViewResolver resolver = new TilesViewResolver();
        return resolver;
    }

    @Bean
    public TilesConfigurer tileConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();
        tilesConfigurer.setDefinitions("/WEB-INF/tiles/*.xml");
        return tilesConfigurer;
    }

//    @Bean
    public ResourceBundleMessageSource getResourceBundleMessageSource() {
        ResourceBundleMessageSource bundle = new ResourceBundleMessageSource();
//        bundle.set
        return bundle;
    }

    @Bean
    public LocalValidatorFactoryBean initDefaultValidator() {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        return validator;
    }
}
