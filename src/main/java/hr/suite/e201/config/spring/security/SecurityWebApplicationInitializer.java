package hr.suite.e201.config.spring.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}
