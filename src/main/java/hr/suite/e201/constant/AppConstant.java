package hr.suite.e201.constant;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
public interface AppConstant {

    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
