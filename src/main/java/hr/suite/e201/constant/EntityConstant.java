package hr.suite.e201.constant;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
public class EntityConstant {

    public static final String EMPLOYEE_ENTITY = "employee";
    public static final String CREATED_BY_ENTITY = "createdBy";
    public static final String UPDATED_BY_ENTITY = "updatedBy";

    public static final String LOOKUP_REQUIREMENT_ENTITY = "lookupRequirement";
}
