package hr.suite.e201.controller;

import hr.suite.e201.bean.ResponseBean;
import hr.suite.e201.model.employee.EmployeeRequirement;
import hr.suite.e201.model.lookup.LookupRequirement;
import hr.suite.e201.service.EmployeeService;
import hr.suite.e201.service.LookupService;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
@Component
public class BaseController {

    private static final Logger LOGGER = Logger.getLogger(BaseController.class);
    @Autowired
    protected LocalValidatorFactoryBean defaultValidator;
    @Autowired
    protected EmployeeService employeeService;
    @Autowired
    protected LookupService lookupService;

    protected List<LookupRequirement> getRequirements(Integer id) {
        List<LookupRequirement> requirements = lookupService.getRequirements();
        List<EmployeeRequirement> temp = employeeService.getEmployeeRequirements(id);

        for (LookupRequirement requirement : requirements) {
            for (EmployeeRequirement e : temp) {
                if (requirement.getRequirementId().equals(e.getLookupRequirement().getRequirementId())) {
                    Set<EmployeeRequirement> a = new HashSet<>();
                    a.add(e);
                    requirement.setEmployeeRequirements(a);
                }
            }
        }

        return requirements;
    }

    protected ResponseBean constructItems(List<?> items) {
        ResponseBean responseBean = new ResponseBean();
        responseBean.getData().put("items", items);
        return responseBean;
    }

    protected ResponseEntity<byte[]> downloadPDF(String filename, byte[] contents) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));

        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        return new ResponseEntity<>(contents, headers, HttpStatus.OK);
    }

    protected void generatePDF(HttpServletResponse response, byte[] contents) {
        try {
            response.setContentType("application/pdf");
            OutputStream out = response.getOutputStream();
            out.write(contents);
        } catch (IOException ex) {
            LOGGER.error(ex);
        }
    }

    protected byte[] generateImageNotFound() {
        return new byte[0];
    }
}
