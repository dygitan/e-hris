package hr.suite.e201.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
@Controller
@RequestMapping("/secured/*")
public class Dashboard {

    @RequestMapping
    public String dashboard() {
        return "dashboard";
    }
}
