package hr.suite.e201.controller;

import hr.suite.e201.model.lookup.LookupPosition;
import hr.suite.e201.model.lookup.LookupRequirement;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
@Controller
@RequestMapping("/secured/lookup/*")
public class LookupController extends BaseController {

    @ResponseBody
    @RequestMapping(value = "json/positions/{key}")
    public List<LookupPosition> getJsonPositions(@PathVariable(value = "key") Integer departmentId) {
        return lookupService.getPositions(departmentId);
    }

    @ResponseBody
    @RequestMapping(value = "json/requirements")
    public List<LookupRequirement> getJsonRequirements() {
        return lookupService.getRequirements();
    }
}
