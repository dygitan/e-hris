package hr.suite.e201.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
@Controller
public class UnsecureController {

    @RequestMapping("/*")
    public String index() {
//        return "redirect:secured/dashboard";
        return "redirect:secured/employee/view/all";
    }

    @RequestMapping("login")
    public String login() {
        return "login";
    }
}
