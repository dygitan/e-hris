package hr.suite.e201.controller.employee;

import hr.suite.e201.bean.ResponseBean;
import hr.suite.e201.builder.ValidatorBuilder;
import hr.suite.e201.controller.BaseController;
import hr.suite.e201.model.employee.Employee;
import hr.suite.e201.model.employee.EmployeeLegal;
import hr.suite.e201.model.employee.EmployeeMovement;
import hr.suite.e201.model.employee.EmployeeSalary;
import hr.suite.e201.model.lookup.LookupRequirement;
import hr.suite.e201.utility.SecurityUtility;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
@Controller
@RequestMapping("/secured/employee/*")
public class EmployeeController extends BaseController {

    private static final Logger LOGGER = Logger.getLogger(EmployeeController.class);
    @Autowired
    private ValidatorBuilder validatorBuilder;

    @InitBinder
    public void initBinder(HttpServletRequest request, WebDataBinder binder) {
        LOGGER.info("Init binder for [" + binder.getTarget() + "]");

        if (binder.getTarget() != null) {
            Validator validator = validatorBuilder.getValidator(binder.getTarget());

            if (validator != null) {
                LOGGER.debug("Binder validator [" + validator + "]");
                binder.setValidator(validator);
            }
        }
    }

    @RequestMapping("new")
    public String viewCreate(ModelMap model) {
        initFormData(model, null);
        model.addAttribute("employee", new Employee());
        return "manageEmployee";
    }

    @RequestMapping("view/{param}")
    public String view(ModelMap model, @PathVariable String param) {
        Integer primaryId = SecurityUtility.decryptId(param);

        if ("all".equalsIgnoreCase(param)) {
            model.addAttribute("departments", lookupService.getDepartments());
            return "viewEmployees";
        } else if (primaryId > 0) {
            Employee employee = employeeService.findById(primaryId);
            model.addAttribute("employee", employee);
            initFormData(model, employee.getLookupPosition().getLookupDepartment().getDepartmentId());
            return "manageEmployee";
        } else {
//            return "redirect:/secured/employee/view/all";
            return "employeeNotFound";
        }
    }

    @RequestMapping(value = "save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseBean saveEmployee(@RequestBody @Validated Employee entity, BindingResult binding) {
        ResponseBean responseBean = new ResponseBean();

        if (!binding.hasErrors()) {
            employeeService.saveOrUpdate(entity);

            responseBean.getData().put("encryptedKey", entity.getEncryptedKey());
            responseBean.getData().put("createdBy", entity.getCreatedBy());
            responseBean.getData().put("createdDate", entity.getCreatedDate());
        } else {
            responseBean.setErrors(binding.getFieldErrors());
        }

        return responseBean;

    }

    @RequestMapping(value = "save/movement", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseBean saveMovement(@RequestBody @Validated EmployeeMovement entity, BindingResult binding) {
        ResponseBean responseBean = new ResponseBean();

        if (!binding.hasErrors()) {
            employeeService.saveOrUpdate(entity);

            responseBean.getData().put("movements",
                    employeeService.getEmployeeMovements(entity.getEmployee().getPrimaryId()));
        } else {
            responseBean.setErrors(binding.getFieldErrors());
        }

        return responseBean;
    }

    @RequestMapping(value = "save/salary", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseBean saveSalary(@RequestBody @Validated EmployeeSalary entity, BindingResult binding) {
        ResponseBean responseBean = new ResponseBean();

        if (!binding.hasErrors()) {
            employeeService.saveOrUpdate(entity);

            responseBean.getData().put("salaries",
                    employeeService.getEmployeeSalaries(entity.getEmployee().getPrimaryId()));
        } else {
            responseBean.setErrors(binding.getFieldErrors());
        }

        return responseBean;
    }

    @ResponseBody
    @RequestMapping(value = "json/all")
    public List<Employee> getJsonEmployees(
            @RequestParam(value = "q", required = false, defaultValue = "") String query,
            @RequestParam(value = "d", required = false, defaultValue = "0") Integer departmentId,
            @RequestParam(value = "p", required = false, defaultValue = "0") Integer positionId) {
        LOGGER.debug("Q=" + query + " > D=" + departmentId + " > P=" + positionId);
        return employeeService.search(query, departmentId, positionId);
    }

    @ResponseBody
    @RequestMapping(value = "json/legal/{key}")
    public List<EmployeeLegal> getJsonEmployeeLegal(@PathVariable(value = "key") String encryptedKey) {
        return employeeService.getEmployeeLegal(SecurityUtility.decryptId(encryptedKey));
    }

    @ResponseBody
    @RequestMapping(value = "json/movements/{key}")
    public List<EmployeeMovement> getJsonEmployeeMovements(@PathVariable(value = "key") String encryptedKey) {
        return employeeService.getEmployeeMovements(SecurityUtility.decryptId(encryptedKey));
    }

    @ResponseBody
    @RequestMapping(value = "json/requirements/{key}")
    public List<LookupRequirement> getJsonEmployeeRequirements(@PathVariable(value = "key") String encryptedKey) {
        return getRequirements(SecurityUtility.decryptId(encryptedKey));
    }

    @ResponseBody
    @RequestMapping(value = "json/salaries/{key}")
    public List<EmployeeSalary> getJsonEmployeeSalaries(@PathVariable(value = "key") String encryptedKey) {
        return employeeService.getEmployeeSalaries(SecurityUtility.decryptId(encryptedKey));
    }

    private void initFormData(ModelMap model, Integer departmentId) {
        model.addAttribute("countries", lookupService.getCountries());
        model.addAttribute("nationalities", lookupService.getNationalities());

        model.addAttribute("departments", lookupService.getDepartments());

        if (departmentId != null) {
            model.addAttribute("positions", lookupService.getPositions(departmentId));
        }
    }
}
