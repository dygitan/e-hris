package hr.suite.e201.controller.employee;

import hr.suite.e201.bean.PropertyEditorBean;
import hr.suite.e201.bean.ResponseBean;
import hr.suite.e201.bean.employee.upload.LegalUploadBean;
import hr.suite.e201.bean.employee.upload.RequirementUploadBean;
import hr.suite.e201.controller.BaseController;
import hr.suite.e201.model.employee.EmployeeLegal;
import hr.suite.e201.model.employee.EmployeeRequirement;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Patrick Tan
 * @since Dec 22, 2014
 */
@Controller
@RequestMapping("/secured/upload/employee/*")
public class EmployeeUploadController extends BaseController {

    private static final Logger LOGGER = Logger.getLogger(EmployeeUploadController.class);
    private static final Map<Class, PropertyEditorBean> PROP_EDITOR_MAP = new HashMap<>();

    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        LOGGER.debug("Init binder: " + getClass());        

        if (!StringUtils.isBlank(request.getParameter("legal"))) {
            binder.registerCustomEditor(EmployeeLegal.class,
                    "legal", getPropertyEditorBean(EmployeeLegal.class));
        }

        if (!StringUtils.isBlank(request.getParameter("requirement"))) {
            binder.registerCustomEditor(EmployeeRequirement.class,
                    "requirement", getPropertyEditorBean(EmployeeRequirement.class));
        }

        /**
         * This is for processing list ...
         *
         * binder.registerCustomEditor(List.class, "legalUploadBeans", new
         * PropertyEditorSupport() {
         *
         * @Override public void setAsText(String text) { LOGGER.info("set as
         * text!" + text);
         *
         * ObjectMapper mapper = new ObjectMapper(); mapper.setDateFormat(new
         * SimpleDateFormat(AppConstant.DEFAULT_DATE_TIME_FORMAT));
         *
         * try { TypeFactory typeFactory = mapper.getTypeFactory();
         *
         * List<LegalUploadBean> beans = mapper.readValue(text,
         * typeFactory.constructCollectionType(List.class,
         * LegalUploadBean.class));
         *
         * LOGGER.info("beans > " + beans);
         *
         * setValue(beans); } catch (IOException e) { LOGGER.error("There was an
         * error encountered in parsing data.", e); } } });
         *
         */
    }

    @RequestMapping("legal")
    @ResponseBody
    public ResponseBean uploadLegal(@ModelAttribute("data") LegalUploadBean bean) throws IOException {
        if (bean.getAttachment() != null) {
            bean.getLegal().setAttachment(bean.getAttachment().getBytes());
            bean.getLegal().setAttachmentType(bean.getAttachment().getContentType());
        }

        employeeService.saveOrUpdate(bean.getLegal());

        return constructItems(employeeService.getEmployeeLegal(
                bean.getLegal().getEmployee().getPrimaryId()));
    }

    @RequestMapping("requirement")
    @ResponseBody
    public ResponseBean uploadRequirement(@ModelAttribute("data") RequirementUploadBean bean) throws IOException {
        if (bean.getAttachment() != null) {
            bean.getRequirement().setAttachment(bean.getAttachment().getBytes());
            bean.getRequirement().setAttachmentType(bean.getAttachment().getContentType());
        }

        employeeService.saveOrUpdate(bean.getRequirement());

        return constructItems(getRequirements(
                bean.getRequirement().getEmployee().getPrimaryId()));
    }

    private PropertyEditorBean getPropertyEditorBean(Class clazz) {
        PropertyEditorBean editorBean = PROP_EDITOR_MAP.get(clazz);

        if (editorBean == null) {
            LOGGER.debug("Initialize property editor bean for " + clazz);

            editorBean = new PropertyEditorBean(clazz);
            PROP_EDITOR_MAP.put(clazz, editorBean);
        }

        return editorBean;
    }
}
