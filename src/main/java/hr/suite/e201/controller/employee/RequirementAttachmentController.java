package hr.suite.e201.controller.employee;

import hr.suite.e201.controller.BaseController;
import hr.suite.e201.model.employee.EmployeeRequirement;
import hr.suite.e201.utility.SecurityUtility;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Patrick Tan
 * @since Dec 30, 2014
 */
@Controller
@RequestMapping("/secured/employee/requirement/attachment/*")
public class RequirementAttachmentController extends BaseController {

    @ResponseBody
    @RequestMapping(value = "/pdf/download/{key}")
    public ResponseEntity<byte[]> downloadPDF(@PathVariable(value = "key") String encryptedKey)
            throws IOException {
        String filename = "legal-attachment.pdf";
        byte[] contents = IOUtils.toByteArray(new FileInputStream(
                new File("D:/Scrum Basic.pdf")));
        return downloadPDF(filename, contents);
    }

    @ResponseBody
    @RequestMapping(value = "/image/{key}")
    public byte[] generateImage(@PathVariable(value = "key") String encryptedKey) throws IOException {
        EmployeeRequirement legal = employeeService.findById(EmployeeRequirement.class,
                SecurityUtility.decryptId(encryptedKey));

        if (legal != null && legal.getAttachmentType().startsWith("image/")) {
            return legal.getAttachment();
        } else {
            return generateImageNotFound();
        }
    }

    @RequestMapping(value = "/pdf/{key}")
    public void generatePDF(@PathVariable(value = "key") String encryptedKey,
            HttpServletResponse response) {
        EmployeeRequirement legal = employeeService.findById(EmployeeRequirement.class,
                SecurityUtility.decryptId(encryptedKey));

        byte[] contents = null;

        if (legal != null && "application/pdf".equals(legal.getAttachmentType())) {
            contents = legal.getAttachment();
        }

        generatePDF(response, contents);
    }
}
