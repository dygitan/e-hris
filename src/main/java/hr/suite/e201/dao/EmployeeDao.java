package hr.suite.e201.dao;

import hr.suite.e201.model.employee.Employee;
import java.util.List;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
public interface EmployeeDao extends GenericDao<Employee> {

    List<Employee> search(String queryText, Integer departmentId, Integer positionId);
}
