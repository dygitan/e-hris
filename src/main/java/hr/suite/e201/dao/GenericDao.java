package hr.suite.e201.dao;

import hr.suite.e201.query.QueryBean;
import java.util.List;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 * @param <T>
 */
public interface GenericDao<T> {

    T get(String idName, Integer id);

    T get(QueryBean queryBean);

    Integer getCount(String fieldName);

    List<T> getListByGeneric(QueryBean queryBean);

    List<T> search(String queryText, String... fields);

    void saveOrUpdate(T t);

}
