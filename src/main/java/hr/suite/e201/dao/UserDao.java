package hr.suite.e201.dao;

import hr.suite.e201.model.SecUser;

/**
 *
 * @author Patrick Tan
 */
public interface UserDao extends GenericDao<SecUser> {

    SecUser getUserDetails(String username);
}
