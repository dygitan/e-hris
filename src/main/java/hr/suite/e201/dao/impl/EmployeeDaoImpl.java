package hr.suite.e201.dao.impl;

import hr.suite.e201.dao.EmployeeDao;
import hr.suite.e201.model.employee.Employee;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
@Repository
public class EmployeeDaoImpl extends GenericDaoImpl<Employee> implements EmployeeDao {

    public EmployeeDaoImpl() {
        super(Employee.class);
    }

    @Override
    public List<Employee> search(String queryText, Integer departmentId, Integer positionId) {
        Criteria criteria = getSession().createCriteria(Employee.class);

        criteria.createAlias("lookupPosition", "lookupPosition");
        criteria.createAlias("lookupPosition.lookupDepartment", "lookupDepartment");

        if (!StringUtils.isBlank(queryText)) {
            criteria.add(Restrictions.disjunction(
                    Restrictions.like("lastName", queryText, MatchMode.ANYWHERE),
                    Restrictions.like("firstName", queryText, MatchMode.ANYWHERE),
                    Restrictions.like("middleName", queryText, MatchMode.ANYWHERE)
            ));
        }

        if (positionId != null && positionId > 0) {
            criteria.add(Restrictions.eq("lookupPosition.positionId", positionId));
        } else {
            if (departmentId != null && departmentId > 0) {
                criteria.add(Restrictions.eq("lookupDepartment.departmentId", departmentId));
            }
        }

        criteria.addOrder(Order.asc("lastName"));

        return criteria.list();
    }
}
