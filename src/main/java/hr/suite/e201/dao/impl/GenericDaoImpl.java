package hr.suite.e201.dao.impl;

import hr.suite.e201.dao.GenericDao;
import hr.suite.e201.query.QueryBean;
import hr.suite.e201.query.QueryBuilder;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 * @param <T>
 */
@Repository(value = "genericDao")
public class GenericDaoImpl<T> implements GenericDao<T> {

    @Autowired
    private QueryBuilder queryBuilder;
    @Autowired
    private SessionFactory sessionFactory;
    private Class clz;

    public GenericDaoImpl() {
    }

    public GenericDaoImpl(Class T) {
        this.clz = T;
    }

    @Override
    public T get(String idName, Integer id) {
        Criteria criteria = getSession().createCriteria(clz);
        criteria.add(Restrictions.eq(idName, id));
        return (T) criteria.uniqueResult();
    }

    @Override
    public T get(QueryBean queryBean) {
        return (T) queryBuilder.buildQuery(queryBean).uniqueResult();
    }

    @Override
    public Integer getCount(String fieldName) {
        return 0;
    }

    @Override
    public List<T> getListByGeneric(QueryBean queryBean) {
        return queryBuilder.buildQuery(queryBean).list();
    }

    @Override
    public List<T> search(String queryText, String... fields) {
//        try {
//            Session session = getSession();
//
//            FullTextSession fullTextSession = Search.getFullTextSession(session);
//            fullTextSession.createIndexer().startAndWait();
//        } catch (Exception e) {
//            e.printStackTrace(System.out);
//        }

        FullTextSession fullTextSession = Search.getFullTextSession(getSession());

        org.hibernate.search.query.dsl.QueryBuilder qb = fullTextSession.getSearchFactory()
                .buildQueryBuilder().forEntity(clz).get();

        org.apache.lucene.search.Query query = qb
                .keyword()
                .onFields(fields)
                .matching(queryText)
                .createQuery();

        org.hibernate.Query hibernateQuery
                = fullTextSession.createFullTextQuery(query, clz);

        return hibernateQuery.list();
    }

    @Override
    public void saveOrUpdate(T t) {
        getSession().saveOrUpdate(t);
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
}
