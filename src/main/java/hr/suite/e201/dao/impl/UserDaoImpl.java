package hr.suite.e201.dao.impl;

import hr.suite.e201.dao.UserDao;
import hr.suite.e201.model.SecUser;
import hr.suite.e201.query.QueryBean;
import hr.suite.e201.query.QueryCondition;
import hr.suite.e201.query.QueryCriteria;
import org.hibernate.FetchMode;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
@Repository("userDao")
public class UserDaoImpl extends GenericDaoImpl<SecUser> implements UserDao {

    public UserDaoImpl() {
        super(SecUser.class);
    }

    @Override
    public SecUser getUserDetails(String username) {
        QueryBean queryBean = new QueryBean(SecUser.class);
        
        queryBean.addCriteria(new QueryCriteria("secUserRoles.lookupUserRole", "lookupUserRole"));
        queryBean.addCriteria(new QueryCriteria("secUserRoles", FetchMode.JOIN));

        queryBean.addCondition(new QueryCondition("username", username));

        return get(queryBean);
    }

}
