package hr.suite.e201.enums;

/**
 *
 * @author Patrick Tan
 * @since Dec 21, 2014
 */
public enum MaritalStatusEnum {

    ANNULLED(1, "Annulled"),
    MARRIED(2, "Married"),
    SEPARATED(3, "Separated"),
    SINGLE(4, "Single"),
    WIDOWED(5, "Widowed");

    private int id;
    private String label;

    private MaritalStatusEnum(int id, String label) {
        this.id = id;
        this.label = label;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }
}
