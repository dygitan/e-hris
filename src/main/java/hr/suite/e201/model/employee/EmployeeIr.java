package hr.suite.e201.model.employee;
// Generated Dec 20, 2014 10:27:59 AM by Hibernate Tools 3.6.0

import hr.suite.e201.model.BaseModel;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * EmployeeIr generated by hbm2java
 */
@Entity
@Table(name = "employee_ir")
@AttributeOverride(name = "primaryId", column = @Column(name = "ir_id"))
public class EmployeeIr extends BaseModel implements java.io.Serializable {

    private Employee employee;
    private String reportedBy;
    private Date incidentReportDate;
    private Date nteReleaseDate;
    private Date nteReceiveDate;
    private Date explanationReceiveDate;
    private String actionsTaken;
    private String remarks;
    private Set<EmployeeIrOffense> employeeIrOffenses = new HashSet<>(0);

    public EmployeeIr() {
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false)
    public Employee getEmployee() {
        return this.employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Column(name = "reported_by", length = 45)
    public String getReportedBy() {
        return this.reportedBy;
    }

    public void setReportedBy(String reportedBy) {
        this.reportedBy = reportedBy;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "incident_report_date", length = 10)
    public Date getIncidentReportDate() {
        return this.incidentReportDate;
    }

    public void setIncidentReportDate(Date incidentReportDate) {
        this.incidentReportDate = incidentReportDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "nte_release_date", length = 10)
    public Date getNteReleaseDate() {
        return this.nteReleaseDate;
    }

    public void setNteReleaseDate(Date nteReleaseDate) {
        this.nteReleaseDate = nteReleaseDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "nte_receive_date", length = 10)
    public Date getNteReceiveDate() {
        return this.nteReceiveDate;
    }

    public void setNteReceiveDate(Date nteReceiveDate) {
        this.nteReceiveDate = nteReceiveDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "explanation_receive_date", length = 10)
    public Date getExplanationReceiveDate() {
        return this.explanationReceiveDate;
    }

    public void setExplanationReceiveDate(Date explanationReceiveDate) {
        this.explanationReceiveDate = explanationReceiveDate;
    }

    @Column(name = "actions_taken", length = 2000)
    public String getActionsTaken() {
        return this.actionsTaken;
    }

    public void setActionsTaken(String actionsTaken) {
        this.actionsTaken = actionsTaken;
    }

    @Column(name = "remarks", length = 1000)
    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employeeIr")
    public Set<EmployeeIrOffense> getEmployeeIrOffenses() {
        return this.employeeIrOffenses;
    }

    public void setEmployeeIrOffenses(Set<EmployeeIrOffense> employeeIrOffenses) {
        this.employeeIrOffenses = employeeIrOffenses;
    }
}
