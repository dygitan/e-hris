package hr.suite.e201.model.employee;
// Generated Jan 2, 2015 9:08:48 PM by Hibernate Tools 3.6.0

import com.fasterxml.jackson.annotation.JsonFormat;
import hr.suite.e201.constant.AppConstant;
import hr.suite.e201.model.BaseModel;
import hr.suite.e201.model.lookup.LookupPosition;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 * EmployeeMovement generated by hbm2java
 */
@Entity
@Table(name = "employee_movement")
@AttributeOverride(name = "primaryId", column = @Column(name = "movement_id"))
public class EmployeeMovement extends BaseModel implements java.io.Serializable {

    private Employee employee;
    private LookupPosition newLookupPosition;
    private LookupPosition currentLookupPosition;
    @JsonFormat(pattern = AppConstant.DEFAULT_DATE_FORMAT, shape = JsonFormat.Shape.STRING)
    @NotNull
    @Future
    private Date effectiveDate;
    @NotBlank
    @Size(max = 5000)
    private String remarks;

    public EmployeeMovement() {
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false)
    public Employee getEmployee() {
        return this.employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "new_position_id", nullable = false)
    public LookupPosition getNewLookupPosition() {
        return newLookupPosition;
    }

    public void setNewLookupPosition(LookupPosition newLookupPosition) {
        this.newLookupPosition = newLookupPosition;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "current_position_id", nullable = false)
    public LookupPosition getCurrentLookupPosition() {
        return currentLookupPosition;
    }

    public void setCurrentLookupPosition(LookupPosition currentLookupPosition) {
        this.currentLookupPosition = currentLookupPosition;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "effective_date", nullable = false, length = 10)
    public Date getEffectiveDate() {
        return this.effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    @Column(name = "remarks", length = 500)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
