package hr.suite.e201.query;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.sql.JoinType;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
public class QueryBean {

    private Class cls;
    private List<QueryCriteria> model = new ArrayList<>();
    private List<QueryCondition> conditions = new ArrayList<>();
    private List<QueryOrdering> ordering = new ArrayList<>();

    public QueryBean() {
    }

    public QueryBean(Class cls) {
        this.cls = cls;
    }

    public QueryBean(Class cls, List<QueryCriteria> model) {
        this.cls = cls;
        this.model = model;
    }

    public Class getCls() {
        return cls;
    }

    public void setCls(Class cls) {
        this.cls = cls;
    }

    public List<QueryCriteria> getModel() {
        return model;
    }

    public void setModel(List<QueryCriteria> model) {
        this.model = model;
    }

    public List<QueryCondition> getConditions() {
        return conditions;
    }

    public void setConditions(List<QueryCondition> conditions) {
        this.conditions = conditions;
    }

    public List<QueryOrdering> getOrdering() {
        return ordering;
    }

    public void setOrdering(List<QueryOrdering> ordering) {
        this.ordering = ordering;
    }

    public void addCondition(String field, Object value) {
        conditions.add(new QueryCondition(field, value));
    }

    public void addCondition(QueryCondition condition) {
        conditions.add(condition);
    }

    public void addCriteria(String referenceName, String aliasName) {
        model.add(new QueryCriteria(referenceName, aliasName));
    }

    public void addCriteria(String referenceName, String aliasName, JoinType joinType) {
        model.add(new QueryCriteria(referenceName, aliasName, joinType));
    }

    public void addCriteria(QueryCriteria criteria) {
        model.add(criteria);
    }

    public void addOrdering(String field, QueryOrderType order) {
        this.ordering.add(new QueryOrdering(field, order));
    }

    public void addOrdering(QueryOrdering ordering) {
        this.ordering.add(ordering);
    }
}
