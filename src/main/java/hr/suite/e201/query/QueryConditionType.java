package hr.suite.e201.query;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
public enum QueryConditionType {

    EQUAL, NOT_EQUAL, LE, LIKE;

    private QueryConditionType() {
    }

}
