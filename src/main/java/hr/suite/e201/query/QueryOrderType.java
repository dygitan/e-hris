package hr.suite.e201.query;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
public enum QueryOrderType {

    ASC, DESC;

    private QueryOrderType() {
    }

}
