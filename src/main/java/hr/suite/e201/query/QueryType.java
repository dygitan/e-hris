package hr.suite.e201.query;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
public enum QueryType {

    FETCH, ALIAS;

    private QueryType() {
    }

}
