package hr.suite.e201.service;

import hr.suite.e201.bean.employee.EmployeeRequirementSummary;
import hr.suite.e201.model.employee.Employee;
import hr.suite.e201.model.employee.EmployeeLegal;
import hr.suite.e201.model.employee.EmployeeMovement;
import hr.suite.e201.model.employee.EmployeeRequirement;
import hr.suite.e201.model.employee.EmployeeSalary;
import java.util.List;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
public interface EmployeeService extends GenericService<Employee> {

    List<EmployeeRequirement> getEmployeeRequirements(Integer id);

    List<EmployeeRequirementSummary> getEmployeeRequirementsSummary(Integer id);

    List<EmployeeLegal> getEmployeeLegal(Integer id);

    List<EmployeeMovement> getEmployeeMovements(Integer id);

    List<EmployeeSalary> getEmployeeSalaries(Integer id);

    List<Employee> search(String queryText, Integer departmentId, Integer positionId);
}
