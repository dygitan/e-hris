package hr.suite.e201.service;

import hr.suite.e201.bean.search.SearchBean;
import java.util.List;

/**
 *
 * @author Patrick Tan
 * @param <T>
 * @since Dec 20, 2014
 */
public interface GenericService<T> {

    T findById(Integer id);

    <S> S findById(Class<S> clazz, Integer id);

    List<T> find(Class<? extends SearchBean> searchBean);

    List<T> search(String queryText);

    void saveOrUpdate(Object entity);
}
