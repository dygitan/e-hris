package hr.suite.e201.service;

import hr.suite.e201.model.lookup.LookupCountry;
import hr.suite.e201.model.lookup.LookupDepartment;
import hr.suite.e201.model.lookup.LookupNationality;
import hr.suite.e201.model.lookup.LookupPosition;
import hr.suite.e201.model.lookup.LookupRequirement;
import java.util.List;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
public interface LookupService {

    List<LookupCountry> getCountries();

    List<LookupDepartment> getDepartments();

    List<LookupNationality> getNationalities();

    List<LookupPosition> getPositions();

    List<LookupPosition> getPositions(Integer departmentId);

    List<LookupRequirement> getRequirements();
}
