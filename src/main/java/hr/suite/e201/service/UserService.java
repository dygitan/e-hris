package hr.suite.e201.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
public interface UserService extends UserDetailsService {
}
