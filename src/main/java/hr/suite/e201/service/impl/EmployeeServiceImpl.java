package hr.suite.e201.service.impl;

import hr.suite.e201.bean.employee.EmployeeRequirementSummary;
import hr.suite.e201.bean.search.SearchBean;
import hr.suite.e201.constant.EntityConstant;
import hr.suite.e201.dao.EmployeeDao;
import hr.suite.e201.dao.GenericDao;
import hr.suite.e201.model.employee.Employee;
import hr.suite.e201.model.employee.EmployeeLegal;
import hr.suite.e201.model.employee.EmployeeMovement;
import hr.suite.e201.model.employee.EmployeeRequirement;
import hr.suite.e201.model.employee.EmployeeSalary;
import hr.suite.e201.query.QueryBean;
import hr.suite.e201.query.QueryCondition;
import hr.suite.e201.query.QueryConditionType;
import hr.suite.e201.query.QueryCriteria;
import hr.suite.e201.query.QueryOrderType;
import hr.suite.e201.service.EmployeeService;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.FetchMode;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private GenericDao genericDao;

    @Transactional(readOnly = true)
    @Override
    public Employee findById(Integer id) {
        QueryBean queryBean = initQueryBean();
        queryBean.addCondition("primaryId", id);
        return employeeDao.get(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public <S> S findById(Class<S> clazz, Integer id) {
        QueryBean queryBean = new QueryBean(clazz);
        queryBean.addCondition("primaryId", id);
        return (S) genericDao.get(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Employee> find(Class<? extends SearchBean> searchBean) {
        QueryBean queryBean = initQueryBean();

        queryBean.addOrdering("lastName", QueryOrderType.ASC);

        return employeeDao.getListByGeneric(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Employee> search(String queryText) {
        return employeeDao.search(queryText, 1, 1);
    }

    @Transactional(readOnly = true)
    @Override
    public List<EmployeeRequirement> getEmployeeRequirements(Integer id) {
        QueryBean queryBean = new QueryBean(EmployeeRequirement.class);

        queryBean.addCriteria(EntityConstant.EMPLOYEE_ENTITY, "employee");
        queryBean.addCriteria(EntityConstant.LOOKUP_REQUIREMENT_ENTITY, "lookupRequirement");
        queryBean.addCriteria(EntityConstant.CREATED_BY_ENTITY, "createdBy");
        queryBean.addCriteria(EntityConstant.UPDATED_BY_ENTITY, "updatedBy", JoinType.LEFT_OUTER_JOIN);

        queryBean.addCondition("employee.primaryId", id);

        return genericDao.getListByGeneric(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public List<EmployeeRequirementSummary> getEmployeeRequirementsSummary(Integer id) {
        QueryBean queryBean = new QueryBean(Employee.class);

        queryBean.addCriteria(new QueryCriteria("employeeRequirements", FetchMode.JOIN));

        List<Employee> employees = employeeDao.getListByGeneric(queryBean);

        List<EmployeeRequirementSummary> s = new ArrayList<>();

        for (Employee e : employees) {
            if (e.getEmployeeRequirements().isEmpty() || e.getEmployeeRequirements().size() < 21) {
                EmployeeRequirementSummary summary = new EmployeeRequirementSummary();
                summary.setEmployee(e);
                s.add(summary);
            }
        }

        return s;
    }

    @Transactional(readOnly = true)
    @Override
    public List<EmployeeLegal> getEmployeeLegal(Integer id) {
        QueryBean queryBean = new QueryBean(EmployeeLegal.class);

        queryBean.addCriteria(EntityConstant.EMPLOYEE_ENTITY, "employee");
        queryBean.addCriteria(EntityConstant.CREATED_BY_ENTITY, "createdBy");
        queryBean.addCriteria(EntityConstant.UPDATED_BY_ENTITY, "updatedBy", JoinType.LEFT_OUTER_JOIN);

        queryBean.addCondition("employee.primaryId", id);
        queryBean.addOrdering("createdDate", QueryOrderType.DESC);

        return genericDao.getListByGeneric(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public List<EmployeeMovement> getEmployeeMovements(Integer id) {
        QueryBean queryBean = new QueryBean(EmployeeMovement.class);

        queryBean.addCriteria(EntityConstant.EMPLOYEE_ENTITY, "employee");
        queryBean.addCriteria("newLookupPosition", "newLookupPosition");
        queryBean.addCriteria("newLookupPosition.lookupDepartment", "newPositionDept");
        queryBean.addCriteria("currentLookupPosition", "currentLookupPosition");
        queryBean.addCriteria("currentLookupPosition.lookupDepartment", "currPositionDept");
        queryBean.addCriteria(EntityConstant.CREATED_BY_ENTITY, "createdBy");
        queryBean.addCriteria(EntityConstant.UPDATED_BY_ENTITY, "updatedBy", JoinType.LEFT_OUTER_JOIN);

        queryBean.addCondition("employee.primaryId", id);
        queryBean.addOrdering("createdDate", QueryOrderType.DESC);

        return genericDao.getListByGeneric(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public List<EmployeeSalary> getEmployeeSalaries(Integer id) {
        QueryBean queryBean = new QueryBean(EmployeeSalary.class);

        queryBean.addCriteria(EntityConstant.EMPLOYEE_ENTITY, "employee");
        queryBean.addCriteria(EntityConstant.CREATED_BY_ENTITY, "createdBy");
        queryBean.addCriteria(EntityConstant.UPDATED_BY_ENTITY, "updatedBy", JoinType.LEFT_OUTER_JOIN);

        queryBean.addCondition("employee.primaryId", id);
        queryBean.addOrdering("effectiveDate", QueryOrderType.DESC);

        return genericDao.getListByGeneric(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Employee> search(String queryText, Integer departmentId, Integer positionId) {
        return employeeDao.search(queryText, departmentId, positionId);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveOrUpdate(Object entity) {
        genericDao.saveOrUpdate(entity);
    }

    private QueryBean initQueryBean() {
        QueryBean queryBean = new QueryBean(Employee.class);

        queryBean.addCriteria(new QueryCriteria("lookupCountry", "lookupCountry"));
        queryBean.addCriteria(new QueryCriteria("lookupNationality", "lookupNationality"));
        queryBean.addCriteria(new QueryCriteria("lookupPosition", "lookupPosition"));
        queryBean.addCriteria(new QueryCriteria("lookupPosition.lookupDepartment", "lookupDepartmentx"));

        queryBean.addCriteria(new QueryCriteria("createdBy", "createdBy"));
        queryBean.addCriteria(new QueryCriteria("updatedBy", "updatedBy", JoinType.LEFT_OUTER_JOIN));

        return queryBean;
    }
}
