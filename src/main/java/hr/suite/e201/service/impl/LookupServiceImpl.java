package hr.suite.e201.service.impl;

import hr.suite.e201.dao.GenericDao;
import hr.suite.e201.model.lookup.LookupCountry;
import hr.suite.e201.model.lookup.LookupDepartment;
import hr.suite.e201.model.lookup.LookupNationality;
import hr.suite.e201.model.lookup.LookupPosition;
import hr.suite.e201.model.lookup.LookupRequirement;
import hr.suite.e201.query.QueryBean;
import hr.suite.e201.query.QueryOrderType;
import hr.suite.e201.service.LookupService;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
@Service
@EnableTransactionManagement
public class LookupServiceImpl implements LookupService {

    private static final Logger LOGGER = Logger.getLogger(LookupServiceImpl.class);
    @Autowired
    private GenericDao genericDao;

    @Transactional(readOnly = true)
    @Override
    public List<LookupCountry> getCountries() {
        QueryBean queryBean = new QueryBean(LookupCountry.class);
        queryBean.addOrdering("country", QueryOrderType.ASC);
        return genericDao.getListByGeneric(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public List<LookupDepartment> getDepartments() {
        QueryBean queryBean = new QueryBean(LookupDepartment.class);
        queryBean.addOrdering("name", QueryOrderType.ASC);
        return genericDao.getListByGeneric(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public List<LookupNationality> getNationalities() {
        QueryBean queryBean = new QueryBean(LookupNationality.class);
        queryBean.addOrdering("description", QueryOrderType.ASC);
        return genericDao.getListByGeneric(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public List<LookupPosition> getPositions() {
        return getPositions(null);
    }

    @Transactional(readOnly = true)
    @Override
    public List<LookupPosition> getPositions(Integer departmentId) {
        QueryBean queryBean = new QueryBean(LookupPosition.class);

        queryBean.addCriteria("lookupDepartment", "lookupDepartment",
                JoinType.INNER_JOIN);

        if (departmentId != null) {
            queryBean.addCondition("lookupDepartment.departmentId", departmentId);
        }
        
        queryBean.addOrdering("name", QueryOrderType.ASC);

        return genericDao.getListByGeneric(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public List<LookupRequirement> getRequirements() {
        QueryBean queryBean = new QueryBean(LookupRequirement.class);
        queryBean.addOrdering("description", QueryOrderType.ASC);
        return genericDao.getListByGeneric(queryBean);
    }
}
