package hr.suite.e201.service.impl;

import hr.suite.e201.bean.UserDetailsBean;
import hr.suite.e201.dao.UserDao;
import hr.suite.e201.model.SecUser;
import hr.suite.e201.model.SecUserRole;
import hr.suite.e201.service.UserService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

    public UserServiceImpl() {
        Logger.getLogger(UserServiceImpl.class).debug("Initialize user service impl ...");
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetailsBean loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.debug("public UserDetailsBean loadUserByUsername(String username) ... ");

        try {
            LOGGER.debug("enter username ... " + username);

            SecUser user = userDao.getUserDetails(username);

            LOGGER.debug("User >> " + user + " >>> " + user.getSecUserRoles().size());

            List<GrantedAuthority> authorities = buildUserAuthority(user.getSecUserRoles());

            return buildUserForAuthentication(user, authorities);
        } catch (Exception e) {
            LOGGER.debug("Oops! User with username [" + username + "] was not found.", e);

            throw new UsernameNotFoundException("ATSException: Username not found", e);
        }
    }

    private UserDetailsBean buildUserForAuthentication(SecUser user, List<GrantedAuthority> authorities) {
        UserDetailsBean bean = new UserDetailsBean(user.getUsername(), user.getPassword(),
                user.getEnabled(), true, true, true, authorities);

        bean.setSecUser(user);

        return bean;
    }

    private List<GrantedAuthority> buildUserAuthority(Set<SecUserRole> userRoles) {
        Set<GrantedAuthority> setAuths = new HashSet<>();

        for (SecUserRole userRole : userRoles) {
            setAuths.add(new SimpleGrantedAuthority(userRole.getLookupUserRole().getDescription()));
        }

        List<GrantedAuthority> Result = new ArrayList<>(setAuths);

        return Result;
    }
}
