package hr.suite.e201.utility;

import hr.suite.e201.bean.UserDetailsBean;
import java.io.UnsupportedEncodingException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

/**
 *
 * @author Patrick Tan
 * @since Dec 20, 2014
 */
public class SecurityUtility {

    private static final Logger LOGGER = Logger.getLogger(SecurityUtility.class);
    private static final String DEFAULT_USERNAME = "default";

    public static String getCurrentUsername() {
        try {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            return user.getUsername();
        } catch (Exception e) {
            LOGGER.warn("Oops! System was not able to retrieve the current password.", e);
            return null;
        }
    }

    public static UserDetailsBean getCurrentUser() {
        return (UserDetailsBean) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static Integer decryptId(String encryptedKey) {
        Integer primaryId = 0;

        try {
            String username = SecurityUtility.getCurrentUsername();

            if (StringUtils.isBlank(username)) {
                username = DEFAULT_USERNAME;
            }

            String[] decrypted = new String(Base64.decodeBase64(encryptedKey.getBytes())).split("-");

            if (decrypted.length > 1 && username.equals(decrypted[1])) {
                try {
                    primaryId = Integer.parseInt(decrypted[0]);
                } catch (NumberFormatException e) {
                    LOGGER.warn("Oops! Decrypted value is an invalid primary id.", e);
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Oops! System was not able to decrypt the provided encrypted key [" + encryptedKey + "].", e);
        }

        LOGGER.debug("Decrypted ID [" + primaryId + "] from [" + encryptedKey + "]");

        return primaryId;
    }

    public static String encryptKey(Integer primaryId) {
        LOGGER.debug("Encrypt primary key [" + primaryId + "]");

        String encryptedKey = null;

        try {
            StringBuilder sb = new StringBuilder();

            sb.append(primaryId.toString());
            sb.append("-");

            String username = SecurityUtility.getCurrentUsername();

            if (StringUtils.isBlank(username)) {
                username = DEFAULT_USERNAME;
            }

            sb.append(username);

            encryptedKey = new String(Base64.encodeBase64(sb.toString().getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Oops! System was not able to encrypt the primary id [" + primaryId + "]", e);
        }

        return encryptedKey;
    }
}
