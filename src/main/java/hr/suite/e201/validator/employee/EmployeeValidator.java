package hr.suite.e201.validator.employee;

import hr.suite.e201.model.employee.Employee;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 *
 * @author Patrick Tan
 * @since Dec 26, 2014
 */
public class EmployeeValidator implements Validator {

    private static final Logger LOGGER = Logger.getLogger(EmployeeValidator.class);
    private final LocalValidatorFactoryBean defaultValidator;

    public EmployeeValidator(LocalValidatorFactoryBean defaultValidator) {
        this.defaultValidator = defaultValidator;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Employee.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.invokeValidator(defaultValidator, target, errors);

        Employee employee = (Employee) target;

        if (employee.getLookupCountry() == null || employee.getLookupCountry().getCountryId() == null) {
            errors.rejectValue("lookupCountry", "", "is a required field");
        }

        if (employee.getLookupNationality() == null || employee.getLookupNationality().getNationalityId() == null) {
            errors.rejectValue("lookupNationality", "", "is a required field");
        }

        if (employee.getLookupPosition() == null || employee.getLookupPosition().getPositionId() == null) {
            errors.rejectValue("lookupPosition", "", "is a required field");
        }

        if (LOGGER.isDebugEnabled()) {
            for (FieldError e : errors.getFieldErrors()) {
                LOGGER.debug("field [" + e.getField() + "] code [" + e.getCode() + "] [" + e.getDefaultMessage() + "]");
            }
        }
    }
}
