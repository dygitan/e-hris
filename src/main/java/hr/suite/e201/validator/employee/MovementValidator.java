package hr.suite.e201.validator.employee;

import hr.suite.e201.model.employee.EmployeeMovement;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 *
 * @author Patrick Tan
 * @since Jan 3, 2015
 */
public class MovementValidator implements Validator {

    private static final Logger LOGGER = Logger.getLogger(MovementValidator.class);
    private final LocalValidatorFactoryBean defaultValidator;

    public MovementValidator(LocalValidatorFactoryBean defaultValidator) {
        this.defaultValidator = defaultValidator;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return EmployeeMovement.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.invokeValidator(defaultValidator, target, errors);

        EmployeeMovement employeeMovement = (EmployeeMovement) target;

        LOGGER.debug("Validate movement " + employeeMovement.getNewLookupPosition() + " > " + employeeMovement.getCurrentLookupPosition());

        if (employeeMovement.getCurrentLookupPosition() == null || employeeMovement.getCurrentLookupPosition().getPositionId() == null) {
            errors.rejectValue("currentLookupPosition", "", "is a required field");
        }

        if (employeeMovement.getNewLookupPosition() == null || employeeMovement.getNewLookupPosition().getPositionId() == null) {
            errors.rejectValue("newLookupPosition", "", "is a required field");
        }

        if (LOGGER.isDebugEnabled()) {
            for (FieldError e : errors.getFieldErrors()) {
                LOGGER.debug("field [" + e.getField() + "] code [" + e.getCode() + "] [" + e.getDefaultMessage() + "]");
            }
        }
    }
}
