<%-- 
    Document   : taglibs
    Created on : Dec 6, 2014, 6:02:28 PM
    Author     : tanpa
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<c:set var="baseUrl" value="${pageContext.request.contextPath}" />

<script>
    var isDebug = false;
    var csrfToken = '${_csrf.token}';
    var baseUrl = '${pageContext.request.contextPath}';
</script>
