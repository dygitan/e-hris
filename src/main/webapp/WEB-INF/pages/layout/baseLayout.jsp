<%-- 
    Document   : baseLayout
    Created on : Dec 6, 2014, 9:59:16 PM
    Author     : tanpa
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            <tiles:insertAttribute name="title" ignore="true" /> | <tiles:insertAttribute name="appName" ignore="true" />
        </title>

        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <c:set var="req" value="${pageContext.request}" />
        <c:set var="baseUrl" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
        
        <link rel="shortcut icon" href="${baseUrl}/resources/img/favicon.ico">
        <link rel="stylesheet" href="${baseUrl}/resources/css/bootstrap-3.3.1.min.css" />
        <link rel="stylesheet" href="${baseUrl}/resources/css/bootstrap-datepicker.css" />
        <link rel="stylesheet" href="${baseUrl}/resources/css/style.css" />

        <script src="${baseUrl}/resources/js/jquery-1.11.0.min.js" type="text/javascript"></script>
        <script src="${baseUrl}/resources/js/bootstrap-3.3.1.min.js" type="text/javascript"></script>
        <script src="${baseUrl}/resources/js/bootstrap-datepicker.js" type="text/javascript"></script>


        <script src="${baseUrl}/resources/js/1.3.5/angular.min.js" type="text/javascript"></script>
        <script src="${baseUrl}/resources/js/1.3.5/angular-resource.min.js" type="text/javascript"></script>
        <script src="${baseUrl}/resources/js/1.3.5/angular-route.min.js" type="text/javascript"></script>

        <script src="${baseUrl}/resources/js/global.js" type="text/javascript"></script>
        <script src="${baseUrl}/resources/js/app/global.js" type="text/javascript"></script>

        <style>
            body {
                font-size: 12px;
            }
        </style>

    </head>
    <body>
        <tiles:insertAttribute name="navigation" />

        <div class="container-fluid" style="margin-top: 65px" ng-app="globalModule">
            <tiles:insertAttribute name="content" />
            <%@include file="global-modal.jsp" %>
        </div>

        <tiles:insertAttribute name="footer" />
    </body>
</html>
