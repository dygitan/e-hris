<%@include file="../common/taglibs.jsp" %>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="navbar-brand">
                e-HRIS
            </span>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="${baseUrl}/secured/dashboard">
                        <span class="glyphicon glyphicon-dashboard"></span> Dashboard
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <span class="glyphicon glyphicon-file"></span> Employees <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="${baseUrl}/secured/employee/new">New</a>
                        </li>
                        <li>
                            <a href="${baseUrl}/secured/employee/view/all">View All</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="${baseUrl}/user/view">
                        <span class="glyphicon glyphicon-user"></span> Admin
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <span class="glyphicon glyphicon-cog"></span> Settings <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="${baseUrl}">Users</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="${baseUrl}">Departments & Positions</a>
                        </li>
                        <li>
                            <a href="${baseUrl}">Requirements</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="${baseUrl}/logout">
                        <span class="glyphicon glyphicon-log-out"></span> Logout
                    </a>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="display: none">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">RPO Tracking System</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="${baseUrl}/secured/dashboard">
                        <span class="glyphicon glyphicon-dashboard"></span> Dashboard
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
                        MRFs <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="${baseUrl}/secured/mrf/new">New</a>
                        </li>
                        <li>
                            <a href="${baseUrl}/secured/mrf/view/all">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
                        Clients <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="${baseUrl}/secured/client/new">New</a>
                        </li>
                        <li>
                            <a href="${baseUrl}/secured/client/view/all">View All</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="${request.contextPath}/secured/">
                        <span class="glyphicon glyphicon-cog"></span> Settings
                    </a>
                </li>
                <li>
                    <a href="${baseUrl}/secured/users/view">

                        <span class="glyphicon glyphicon-user"></span> Admin
                    </a>
                </li>
                <li>
                    <a href="#">Help</a>
                </li>
                <li>
                    <a href="${baseUrl}/logout">
                        <span class="glyphicon glyphicon-log-out"></span> Logout
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>


