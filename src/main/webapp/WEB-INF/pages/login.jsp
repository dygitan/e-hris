<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-3.3.1.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/login.css" />

        <title>
            <tiles:insertAttribute name="appName" ignore="true" /> | Login
        </title>
    </head>
    <body>
        <div class="container">
            <form name="loginForm" class="form-signin" role="form" action="${pageContext.request.contextPath}/login" method="POST">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <tiles:insertAttribute name="appName" ignore="true" /> | Login
                    </div>
                    <div class="panel-body">
                        <h2 class="form-signin-heading" style="margin: 0px; padding: 10px 0px; margin-bottom: 5px; font-size: 15px; font-weight: 600">WELCOME</h2>
                        <input type="text" name="username" class="form-control" placeholder="Username" required autofocus="autofocus" />
                        <input type="password" name="password" class="form-control" placeholder="Password" required="required" />

                        <div class="row">
                            <div class="col-md-7 text-left" style="padding: 0px; padding-left: 15px;">
                                <hr style="margin: 5px 0px 5px;"/>
                                <a style="font-size: 11px">Forgot password?</a>
                            </div>
                            <div class="col-md-5 text-right" style="padding: 0px; padding-right: 15px">
                                <button class="btn btn-success btn-circle" type="submit">
                                    Sign in <span class="glyphicon glyphicon-log-in"></span>
                                </button>
                            </div>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
