<div class="row">
    <div class="col-lg-3 hidden">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <%@include file="pending-requirements.jsp" %>
            <%@include file="top-performers.jsp" %>
            <%@include file="top-offenders.jsp" %>
        </div>
    </div>
</div>