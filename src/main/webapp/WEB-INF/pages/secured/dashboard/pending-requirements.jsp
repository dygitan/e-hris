<%-- 
    Document   : pending-requirements
    Created on : Dec 22, 2014, 11:46:34 AM
    Author     : Patrick Tan
--%>

<div class="panel panel-info">
    <div class="panel-heading" role="tab" id="pendingRequirementsHeading">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#pendingRequirements" aria-expanded="false" aria-controls="pendingRequirements">
            EMPLOYEES WITH PENDING REQUIREMENTS
        </a>
    </div>
    <div id="pendingRequirements" class="panel-collapse collapse" role="tabpanel" aria-labelledby="pendingRequirementsHeading">
        <div class="panel-body">
            EMPLOYEES WITH PENDING REQUIREMENTS HERE
        </div>
    </div>
</div>
