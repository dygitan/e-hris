<%-- 
    Document   : top-performers
    Created on : Dec 22, 2014, 11:49:06 AM
    Author     : Patrick Tan
--%>

<div class="panel panel-success">
    <div class="panel-heading" role="tab" id="topPerformersHeading">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#topPerformers" aria-expanded="false" aria-controls="topPerformers">
            TOP PERFORMERS
        </a>
    </div>
    <div id="topPerformers" class="panel-collapse collapse" role="tabpanel" aria-labelledby="topPerformersHeading">
        <div class="panel-body">
            TOP PERFORMERS HERE
        </div>
    </div>
</div>
