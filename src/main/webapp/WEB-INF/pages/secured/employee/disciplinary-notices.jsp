<%-- 
    Document   : disciplinary-notices
    Created on : Dec 20, 2014, 5:54:17 PM
    Author     : Patrick Tan
--%>

<div class="panel panel-primary" ng-controller="manageNotice">
    <div class="panel-heading" role="tab" id="disciplinaryNoticesHeading">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#disciplinaryNotices" aria-expanded="false" aria-controls="disciplinaryNotices">
            DISCIPLINARY NOTICES
        </a>
    </div>
    <div id="disciplinaryNotices" class="panel-collapse collapse" role="tabpanel" aria-labelledby="disciplinaryNoticesHeading">
        <div class="panel-body">
            <div style="margin-left: 5px">                
                <button ng-click="addNotice()" class="btn btn-sm btn-success">ADD NOTICE</button>
            </div>
            <hr>
        </div>
    </div>

    <!-- start modal -->
    <div id="noticeModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h5 class="modal-title text-primary">DISCIPLINARY NOTICE</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden"                              
                               ng-model="employeeIr.primaryId"   
                               />

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Reported By:</label>
                                <div class="input-group-sm">
                                    <input type="text" 
                                           class="form-control"      
                                           ng-class="errors.newSalary ? 'validation-field' : ''"
                                           ng-model="employeeIr.reportedBy"
                                           required="true"
                                           maxlength="7" />

                                    <div class="text-right">
                                        <span ng-show="errors.reportedBy" class="ng-hide validation-error">{{errors.reportedBy}}</span>
                                    </div>
                                </div>
                            </div>      
                            <div class="form-group">
                                <label for="">Incident Report Date:</label>
                                <div class="input-group-sm">
                                    <input type="text" 
                                           class="form-control datepicker"            
                                           placeholder="YYYY-MM-DD"
                                           maxlength="10"
                                           ng-class="errors.incidentReportDate ? 'validation-field' : ''"
                                           ng-model="employeeIr.incidentReportDate"      
                                           required="true" />

                                    <div class="text-right">
                                        <span ng-show="errors.incidentReportDate" class="ng-hide validation-error">{{errors.incidentReportDate}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">NTE Release Date:</label>
                                <div class="input-group-sm">
                                    <input type="text" 
                                           class="form-control datepicker"
                                           placeholder="YYYY-MM-DD"
                                           ng-class="errors.nteReleaseDate ? 'validation-field' : ''"
                                           ng-model="employeeIr.nteReleaseDate"   
                                           maxlength="10"
                                           required="true" />

                                    <div class="text-right">
                                        <span ng-show="errors.nteReleaseDate" class="ng-hide validation-error">{{errors.nteReleaseDate}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">NTE Receive Date:</label>
                                <div class="input-group-sm">
                                    <input type="text" 
                                           class="form-control datepicker"            
                                           placeholder="YYYY-MM-DD"
                                           maxlength="10"
                                           ng-class="errors.nteReceiveDate ? 'validation-field' : ''"
                                           ng-model="employeeIr.nteReceiveDate"      
                                           required="true" />

                                    <div class="text-right">
                                        <span ng-show="errors.nteReceiveDate" class="ng-hide validation-error">{{errors.nteReceiveDate}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Explanation Receive Date:</label>
                                <div class="input-group-sm">
                                    <input type="text" 
                                           class="form-control datepicker"            
                                           placeholder="YYYY-MM-DD"
                                           maxlength="10"
                                           ng-class="errors.explanationReceiveDate ? 'validation-field' : ''"
                                           ng-model="employeeIr.explanationReceiveDate"      
                                           required="true" />

                                    <div class="text-right">
                                        <span ng-show="errors.explanationReceiveDate" class="ng-hide validation-error">{{errors.explanationReceiveDate}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Remarks:</label>
                                <div class="input-group-sm">
                                    <textarea class="form-control"
                                              ng-class="errors.adjustmentReason ? 'validation-field' : ''"
                                              ng-model="employeeIr.adjustmentReason" >
                                    </textarea>

                                    <div class="text-right">
                                        <span ng-show="errors.adjustmentReason" class="ng-hide validation-error">{{errors.adjustmentReason}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Notice:</label>
                                <div class="input-group-sm">
                                    <input type="file" 
                                           name="nteFile" 
                                           ng-model="nteFile" 
                                           data-rule-required="true" 
                                           accept="image/*, .pdf">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Reply:</label>
                                <div class="input-group-sm">
                                    <input type="file" 
                                           name="replyFile" 
                                           ng-model="replyFile" 
                                           data-rule-required="true" 
                                           accept="image/*, .pdf">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">
                                    Offenses:
                                    <select style="font-weight: normal">
                                        <option>- Severity Level-</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </label>
                                <div class="input-group-sm">
                                    <select class="form-control" 
                                            multiple="true"
                                            style="height: 183px">
                                        <option value="1">Offense 1</option>
                                        <option value="2">Offense 2</option>
                                        <option value="3">Offense 3</option>
                                        <option value="4">Offense 4</option>
                                        <option value="5">Offense 5</option>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label for="">Selected Offenses:</label>
                                <div class="input-group-sm">
                                    <select class="form-control" 
                                            ng-class="errors.newSalary ? 'validation-field' : ''"
                                            ng-model="employeeIr.employeeIrOffenses.lookupOffense.offenseId"
                                            multiple="true"
                                            style="height: 110px">
                                        <option value="1">Offense 1</option>
                                        <option value="2">Offense 2</option>
                                        <option value="3">Offense 3</option>
                                        <option value="4">Offense 4</option>
                                        <option value="5">Offense 5</option>
                                    </select>
                                    <div class="text-right">
                                        <span ng-show="errors.employeeIrOffenses" class="ng-hide validation-error">{{errors.employeeIrOffenses}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">CLOSE</button>
                    <button type="button" class="btn btn-sm btn-primary" ng-click="saveNotice()">SAVE CHANGES</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->
</div>
