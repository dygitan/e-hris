<%-- 
    Document   : employee-movement
    Created on : Dec 20, 2014, 5:54:29 PM
    Author     : Patrick Tan
--%>

<div class="panel panel-primary" ng-controller="manageMovementCtrl">
    <div class="panel-heading" role="tab" id="employeeMovementHeading">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#employeeMovement" aria-expanded="false" aria-controls="employeeMovement" ng-click="initMovements()">
            EMPLOYEE MOVEMENT
        </a>
    </div>
    <div id="employeeMovement" class="panel-collapse collapse" role="tabpanel" aria-labelledby="employeeMovementHeading">
        <div class="panel-body">
            <div style="margin-left: 5px">                
                <button ng-click="manage()" class="btn btn-sm btn-success">
                    <span class="glyphicon glyphicon-edit"></span> NEW RECORD
                </button>
            </div>
            <hr>
            <div style="max-height: 406px; overflow-style: scrollbar; overflow: auto">
                <p style="margin-bottom: 0px" class="ng-hide" ng-show="showLoading">
                    Loading movements information <img src="${baseUrl}/resources/img/loader.gif" />
                </p> 

                <ul id="movementsList" class="summary" ng-show="items.length > 0">
                    <li class="list-group-item" 
                        ng-repeat="movement in items">
                        <div class="row">
                            <div class="col-lg-4">
                                <label>New Position:</label>
                                <span>
                                    {{movement.newLookupPosition.name}}
                                </span>
                                <br>
                                <label>Effective Date:</label>
                                <span>
                                    {{movement.effectiveDate}}
                                </span>
                                <br>
                                <p style="margin-left: 5px">
                                    {{movement.remarks}}
                                </p>
                            </div>
                            <div class="col-lg-4 bordered-left">
                                <label>Previous Position:</label>
                                <span>
                                    {{movement.currentLookupPosition.name}}
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn btn-xs btn-default" ng-click="manage(movement)">
                                    <span class="glyphicon glyphicon-floppy-open"></span> UPDATE
                                </button>
                            </div>
                        </div>
                    </li>
                </ul>

                <p style="margin-bottom: 0px" 
                   class="ng-hide" 
                   ng-show="items.length == 0">
                    No movements information available.
                </p>
            </div>
        </div>
    </div>

    <!-- start modal -->
    <div id="movementModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h5 class="modal-title text-primary">EMPLOYEE MOVEMENT</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden"                              
                               ng-model="employeeMovement.encryptedKey"   
                               />

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">New Department:</label>
                                <div class="input-group-sm">
                                    <select class="form-control" 
                                            ng-model="employeeMovement.newLookupPosition.lookupDepartment.departmentId"
                                            ng-change="filterPositions()">
                                        <option ng-selected="!employeeMovement.newPosition.lookupDepartment.departmentId" value="">-- Select --</option>
                                        <c:forEach var="department" items="${requestScope.departments}">
                                            <option value="${department.departmentId}">${department.name}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">
                                    New Position <span class="required-field">*</span>
                                </label>
                                <div class="input-group-sm">
                                    <div class="input-group-sm">
                                        <select class="form-control"
                                                ng-class="errors.newLookupPosition ? 'validation-field' : ''"
                                                ng-model="employeeMovement.newLookupPosition.positionId">
                                            <option value="">-- Select --</option>
                                            <option ng-repeat="position in positions"
                                                    ng-selected="position.positionId == employeeMovement.newLookupPosition.positionId"
                                                    value="{{position.positionId}}">
                                                {{position.name}}
                                            </option>
                                        </select>
                                    </div>

                                    <div class="text-right">
                                        <span ng-show="errors.newLookupPosition" class="ng-hide validation-error">{{errors.newLookupPosition}}</span>
                                    </div>
                                </div>
                            </div>                                
                            <div class="form-group">
                                <label for="">
                                    Effectivity Date <span class="required-field">*</span>
                                </label>
                                <div class="input-group-sm">
                                    <input type="text" 
                                           class="form-control datepicker"
                                           placeholder="YYYY-MM-DD"
                                           ng-class="errors.effectiveDate ? 'validation-field' : ''"
                                           ng-model="employeeMovement.effectiveDate"   
                                           maxlength="10"
                                           required="true" />

                                    <div class="text-right">
                                        <span ng-show="errors.effectiveDate" class="ng-hide validation-error">{{errors.effectiveDate}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Current Department</label>
                                <div class="input-group-sm">
                                    <input type="text" 
                                           class="form-control"                                            
                                           value="{{employee.lookupPosition.lookupDepartment.name}}"
                                           readonly="true"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Current Position:</label>
                                <div class="input-group-sm">
                                    <input type="hidden" 
                                           ng-model="employeeMovement.currentLookupPosition.positionId" 
                                           value="{{employee.lookupPosition.positionId}}"/>

                                    <input type="text" 
                                           class="form-control"          
                                           value="{{employee.lookupPosition.name}}"
                                           readonly="true"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">
                                    Remarks <span class="required-field">*</span>
                                </label>
                                <div class="input-group-sm">
                                    <textarea class="form-control"
                                              ng-class="errors.remarks ? 'validation-field' : ''"
                                              ng-model="employeeMovement.remarks" >
                                    </textarea>

                                    <div class="text-right">
                                        <span ng-show="errors.remarks" class="ng-hide validation-error">{{errors.remarks}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">                    
                        <div class="col-xs-12 text-left">
                            <span class="required-field">Fields marked with * are required.</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">CLOSE</button>
                    <button type="button" class="btn btn-sm btn-primary" ng-click="saveMovement()">SAVE CHANGES</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->
</div>
