<%-- 
    Document   : legal-related
    Created on : Dec 20, 2014, 5:53:00 PM
    Author     : Patrick Tan
--%>



<div class="panel panel-primary" ng-controller="manageLegalCtrl">
    <div class="panel-heading" role="tab" id="legalRelatedHeading">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#legalRelated" aria-expanded="false" aria-controls="legalRelated" ng-click="initLegal()">
            LEGAL DOCUMENTS
        </a>
    </div>
    <div id="legalRelated" class="panel-collapse collapse" role="tabpanel" aria-labelledby="legalRelatedHeading">
        <div class="panel-body">
            <div style="margin-left: 5px">                
                <button ng-click="addLegal()" class="btn btn-sm btn-success">
                    <span class="glyphicon glyphicon-edit"></span> NEW RECORD
                </button>
            </div>
            <hr>
            <div style="max-height: 406px; overflow-style: scrollbar; overflow: auto">
                <p style="margin-bottom: 0px" class="ng-hide" ng-show="showLoading">
                    Loading legal related information <img src="${baseUrl}/resources/img/loader.gif" />
                </p> 
                <ul class="summary">
                    <li  data-toggle="tooltip" 
                         data-placement="bottom" title="Click to edit or view."
                         class="list-group-item" ng-repeat="legal in items">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                <span>
                                    {{legal.remarks}}
                                </span>
                                <br>
                                <button class="btn btn-xs btn-default" 
                                        style="margin-top: 5px"
                                        ng-click="viewAttachment(legal)">
                                    <span class="glyphicon glyphicon-paperclip"></span> VIEW ATTACHMENT
                                </button>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 bordered-left">
                                <span>{{legal.createdBy.username}}</span>
                                <br>
                                <span>{{legal.createdDate}}</span>
                            </div>
                        </div>
                    </li>
                </ul>
                <p style="margin-bottom: 0px" 
                   class="ng-hide" 
                   ng-show="items.length == 0">
                    No legal related information available.
                </p>
            </div>
        </div>
    </div>

    <!-- start modal -->
    <div id="viewLegalAttachmentModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="row" style="padding: 10px">
                    <div class="alert alert-success ng-hide" ng-show="showAttachmentLoader" style="width: 97%;
                         margin: auto;
                         padding: 5px 10px;">
                        <p style="margin-bottom: 0px; font-size: 13px" class="text-success">
                            Please wait, retrieving attachment <img src="${baseUrl}/resources/img/loader.gif" />
                        </p>
                    </div>
                    <div class="col-lg-12 ng-hide attachment-container" ng-show="pdf" >
                        <object data="{{pdf}}" type="application/pdf"></object>
                    </div>
                    <div class="col-lg-12 ng-hide attachment-container" ng-show="image">
                        <img class="img-thumbnail" 
                             style="height: 100%; width: 100%"                              
                             src="{{image}}" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->

    <!-- start modal -->
    <div id="employeeLegalModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="employeeLegalModal" aria-hidden="true">
        <form method="post" enctype="multipart/form-data" ng-submit="upload()" novalidate>
            <div class="modal-dialog">
                <div class="modal-content">                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title text-primary">LEGAL</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Remarks:</label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control"  
                                       ng-model="legal.remarks" 
                                       placeholder="Remarks"
                                       required="true" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Attachment:</label>
                            <div class="input-group-sm">
                                <input type="file" 
                                       name="legalFile" 
                                       ng-model="legalFile" 
                                       data-rule-required="true" 
                                       accept="image/*, .pdf"
                                       id="legalFile">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">CLOSE</button>
                        <button type="subtmit" class="btn btn-sm btn-primary">SAVE CHANGES</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end modal -->
</div>
