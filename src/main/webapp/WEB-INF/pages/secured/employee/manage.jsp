<%-- 
    Document   : manage
    Created on : Dec 20, 2014, 12:23:36 PM
    Author     : Patrick Tan
--%>

<%@page import="hr.suite.e201.enums.EmploymentStatusEnum"%>
<%@page import="hr.suite.e201.enums.GenderEnum"%>
<%@page import="hr.suite.e201.enums.MaritalStatusEnum"%>

<%@include file="../../common/taglibs.jsp" %>

<%
    request.setAttribute("employmentStatuses", EmploymentStatusEnum.values());
    request.setAttribute("genders", GenderEnum.values());
    request.setAttribute("maritalStatuses", MaritalStatusEnum.values());
%>

<link rel="stylesheet" href="${baseUrl}/resources/css/employee.css" />

<div class="row" ng-controller="manageEmployeeCtrl">
    <div class="col-sm-5 col-md-4 col-lg-5">
        <div class="panel panel-primary">
            <div class="panel-heading">EMPLOYEE INFORMATION</div>
            <div class="panel-body">
                <input type="hidden" 
                       class="form-control"                                      
                       ng-model="employee.encryptedKey"      
                       ng-init="employee.encryptedKey = '${requestScope.employee.encryptedKey}'"
                       />
                <input type="hidden" 
                       class="form-control"                                      
                       ng-model="employee.createdBy.username"      
                       ng-init="employee.createdBy.username = '${requestScope.employee.createdBy.username}'"
                       />
                <input type="hidden" 
                       class="form-control"                                      
                       ng-model="employee.createdDate"      
                       ng-init="employee.createdDate = '${requestScope.employee.createdDate}'"
                       />
                
                <div class="row">
                    <div class="col-lg-6">
                        <div class="row" style="margin-bottom: 5px">
                            <div class="col-xs-12 text-left">
                                <strong class="text-primary text-capitalize">
                                    PERSONAL INFORMATION
                                </strong>
                                <hr>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Employee No <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control"      
                                       ng-class="errors.employeeNo ? 'validation-field' : ''"
                                       ng-model="employee.employeeNo"      
                                       ng-init="employee.employeeNo = '${requestScope.employee.employeeNo}'"
                                       required="true" 
                                       maxlength="45"/>

                                <div class="text-right">
                                    <span ng-show="errors.employeeNo" class="ng-hide validation-error">{{errors.employeeNo}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Last Name <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control"    
                                       ng-class="errors.lastName ? 'validation-field' : ''"
                                       ng-model="employee.lastName"      
                                       ng-init="employee.lastName = '${requestScope.employee.lastName}'"
                                       required="true"
                                       maxlength="45" />

                                <div class="text-right">
                                    <span ng-show="errors.lastName" class="ng-hide validation-error">{{errors.lastName}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                First Name <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control"    
                                       ng-class="errors.firstName ? 'validation-field' : ''"
                                       ng-model="employee.firstName"      
                                       ng-init="employee.firstName = '${requestScope.employee.firstName}'"
                                       required="true"
                                       maxlength="45" />
                                <div class="text-right">
                                    <span ng-show="errors.firstName" class="ng-hide validation-error">{{errors.firstName}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Middle Name
                            </label>
                            <div class="input-group-sm">

                                <input type="text" 
                                       class="form-control"                                      
                                       ng-model="employee.middleName"      
                                       ng-init="employee.middleName = '${requestScope.employee.middleName}'"
                                       required="true"
                                       maxlength="45" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Birth Date <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control datepicker"  
                                       placeholder="YYYY-MM-DD"
                                       maxlength="10"
                                       ng-class="errors.birthDate ? 'validation-field' : ''"
                                       ng-model="employee.birthDate"      
                                       ng-init="employee.birthDate = '${requestScope.employee.birthDate}'"
                                       required="true" />

                                <div class="text-right">
                                    <span ng-show="errors.birthDate" class="ng-hide validation-error">{{errors.birthDate}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Gender <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <select class="form-control"
                                        ng-class="errors.gender ? 'validation-field' : ''"
                                        ng-model="employee.gender"
                                        ng-init="employee.gender = '${requestScope.employee.gender}'">
                                    <option ng-selected="employee.gender == 0" value="0">
                                        -- Select --
                                    </option>
                                    <c:forEach var="gender" items="${requestScope.genders}">
                                        <option value="${gender.id}">${gender.label}</option>
                                    </c:forEach>
                                </select>

                                <div class="text-right">
                                    <span ng-show="errors.gender" class="ng-hide validation-error">{{errors.gender}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Marital Status <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <select class="form-control"
                                        ng-class="errors.maritalStatus ? 'validation-field' : ''"
                                        ng-model="employee.maritalStatus"
                                        ng-init="employee.maritalStatus = '${requestScope.employee.maritalStatus}'">
                                    <option ng-selected="employee.maritalStatus == 0" value="0">
                                        -- Select --
                                    </option>
                                    <c:forEach var="maritalStatus" items="${requestScope.maritalStatuses}">
                                        <option value="${maritalStatus.id}">${maritalStatus.label}</option>
                                    </c:forEach>
                                </select>

                                <div class="text-right">
                                    <span ng-show="errors.maritalStatus" class="ng-hide validation-error">{{errors.maritalStatus}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Nationality <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <select class="form-control"
                                        ng-class="errors.lookupNationality ? 'validation-field' : ''"
                                        ng-model="employee.lookupNationality.nationalityId"
                                        ng-init="employee.lookupNationality.nationalityId = '${requestScope.employee.lookupNationality.nationalityId}'">
                                    <option ng-selected="!employee.lookupNationality.nationalityId" value="0">
                                        -- Select --
                                    </option>
                                    <c:forEach var="nationality" items="${requestScope.nationalities}">
                                        <option value="${nationality.nationalityId}">${nationality.description}</option>
                                    </c:forEach>
                                </select>

                                <div class="text-right">
                                    <span ng-show="errors.lookupNationality" class="ng-hide validation-error">{{errors.lookupNationality}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row" style="margin-bottom: 5px">
                            <div class="col-xs-12 text-left">
                                <strong class="text-primary text-capitalize">
                                    CONTACT DETAILS
                                </strong>
                                <hr>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Mobile <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control"   
                                       ng-class="errors.mobile ? 'validation-field' : ''"
                                       ng-model="employee.mobile"      
                                       ng-init="employee.mobile = '${requestScope.employee.mobile}'"
                                       required="true" 
                                       maxlength="15" />

                                <div class="text-right">
                                    <span ng-show="errors.mobile" class="ng-hide validation-error">{{errors.mobile}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Telephone:</label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control"                                      
                                       ng-model="employee.telephone"      
                                       ng-init="employee.telephone = '${requestScope.employee.telephone}'"
                                       required="true"
                                       maxlength="10" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Email <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control"     
                                       ng-class="errors.email ? 'validation-field' : ''"
                                       ng-model="employee.email"      
                                       ng-init="employee.email = '${requestScope.employee.email}'"
                                       required="true" 
                                       maxlength="45" />

                                <div class="text-right">
                                    <span ng-show="errors.email" class="ng-hide validation-error">{{errors.email}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Country <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <select class="form-control"
                                        ng-class="errors.lookupCountry ? 'validation-field' : ''"
                                        ng-model="employee.lookupCountry.countryId"
                                        ng-init="employee.lookupCountry.countryId = '${requestScope.employee.lookupCountry.countryId}'">
                                    <option ng-selected="!employee.lookupCountry.countryId" value="0">
                                        -- Select --
                                    </option>
                                    <c:forEach var="country" items="${requestScope.countries}">
                                        <option value="${country.countryId}">${country.country}</option>
                                    </c:forEach>
                                </select>

                                <div class="text-right">
                                    <span ng-show="errors.lookupCountry" class="ng-hide validation-error">{{errors.lookupCountry}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Address <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <textarea class="form-control"
                                          ng-class="errors.address ? 'validation-field' : ''"
                                          ng-model="employee.address"
                                          ng-init="employee.address = '${requestScope.employee.address}'"
                                          ></textarea>

                                <div class="text-right">
                                    <span ng-show="errors.address" class="ng-hide validation-error">{{errors.address}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 5px">
                            <div class="col-xs-12 text-left">
                                <strong class="text-primary text-capitalize">
                                    EMPLOYMENT DETAILS
                                </strong>
                                <hr>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Department:
                            </label>
                            <div class="input-group-sm">
                                <select class="form-control"                                        
                                        ng-model="employee.lookupPosition.lookupDepartment.departmentId"
                                        ng-init="employee.lookupPosition.lookupDepartment.departmentId = '${requestScope.employee.lookupPosition.lookupDepartment.departmentId}'"
                                        ng-change="filterPositions()">
                                    <option value="">-- Select --</option>
                                    <c:forEach var="department" items="${requestScope.departments}">
                                        <option value="${department.departmentId}">${department.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Position <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="hidden" 
                                       ng-model="employee.lookupPosition.lookupDepartment.name"
                                       ng-init="employee.lookupPosition.lookupDepartment.name = '${requestScope.employee.lookupPosition.lookupDepartment.name}'">
                                <input type="hidden" 
                                       ng-model="employee.lookupPosition.name"
                                       ng-init="employee.lookupPosition.name = '${requestScope.employee.lookupPosition.name}'">

                                <select id="managePositions"
                                        class="form-control"
                                        ng-class="errors.lookupPosition ? 'validation-field' : ''"
                                        ng-model="employee.lookupPosition.positionId"
                                        ng-init="employee.lookupPosition.positionId = '${requestScope.employee.lookupPosition.positionId}'">
                                    <option value="">-- Select --</option>
                                    <option ng-repeat="position in managePositions"
                                            ng-selected="position.positionId == employee.lookupPosition.positionId"
                                            value="{{position.positionId}}">
                                        {{position.name}}
                                    </option>
                                    <c:forEach var="position" items="${requestScope.positions}">
                                        <option value="${position.positionId}">${position.name}</option>
                                    </c:forEach>
                                </select>

                                <div class="text-right">
                                    <span ng-show="errors.lookupPosition" class="ng-hide validation-error">{{errors.lookupPosition}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Status <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <select class="form-control"
                                        ng-class="errors.employmentStatus ? 'validation-field' : ''"
                                        ng-model="employee.employmentStatus"
                                        ng-init="employee.employmentStatus = '${requestScope.employee.employmentStatus}'">
                                    <option ng-selected="employee.employmentStatus == 0" value="0">
                                        -- Select --
                                    </option>
                                    <c:forEach var="employmentStatus" items="${requestScope.employmentStatuses}">
                                        <option value="${employmentStatus.id}">${employmentStatus.label}</option>
                                    </c:forEach>
                                </select>

                                <div class="text-right">
                                    <span ng-show="errors.employmentStatus" class="ng-hide validation-error">{{errors.employmentStatus}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Date Hired <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control datepicker"  
                                       placeholder="YYYY-MM-DD"
                                       maxlength="10"
                                       ng-class="errors.dateHired ? 'validation-field' : ''"
                                       ng-model="employee.dateHired"      
                                       ng-init="employee.dateHired = '${requestScope.employee.dateHired}'"
                                       required="true" />

                                <div class="text-right">
                                    <span ng-show="errors.dateHired" class="ng-hide validation-error">{{errors.dateHired}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="row">                    
                    <div class="col-xs-12 text-left">
                        <span class="required-field">Fields marked with * are required.</span>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <button class="btn btn-success btn-sm" ng-click="save()">SAVE</button>
                        |
                        <button class="btn btn-danger btn-sm">
                            <a href="${baseUrl}/secured/employee/view/all">CANCEL</a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-7 col-md-8 col-lg-6 ng-hide" ng-show="employee.encryptedKey">
        <div id="accordion"  class="panel-group" role="tablist" aria-multiselectable="true">
            <%@include file="salary-summary.jsp" %>
            <%@include file="pre-employment.jsp" %>
            <%@include file="legal-related.jsp" %>
            <%@include file="performance-report.jsp" %>
            <%@include file="disciplinary-notices.jsp" %>
            <%@include file="employee-movement.jsp" %>
            <%@include file="continuing-education.jsp" %>
            <%@include file="commendation.jsp" %>
        </div>
    </div>
</div>

<script src="${baseUrl}/resources/js/app/dataservice.js"></script>
<script src="${baseUrl}/resources/js/app/employee.js"></script>

