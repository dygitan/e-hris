<%-- 
    Document   : not-found
    Created on : Dec 30, 2014, 3:33:06 PM
    Author     : Patrick Tan
--%>

<%@include file="../../common/taglibs.jsp" %>
<div class="text-center" style="margin-top: 50px">
    <strong style="font-size: 20px;">EMPLOYEE RECORD NOT FOUND!</strong>
    <br>
    <a href="${baseUrl}/secured/employee/view/all">Go back to view employees page.</a>
</div>
