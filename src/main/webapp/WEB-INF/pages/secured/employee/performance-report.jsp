<%-- 
    Document   : performance-report
    Created on : Dec 20, 2014, 5:53:14 PM
    Author     : Patrick Tan
--%>

<div class="panel panel-primary">
    <div class="panel-heading" role="tab" id="performanceReportHeading">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#performanceReport" aria-expanded="false" aria-controls="performanceReport">
            PERFORMANCE REPORT
        </a>
    </div>
    <div id="performanceReport" class="panel-collapse collapse" role="tabpanel" aria-labelledby="performanceReportHeading">
        <div class="panel-body">
            PERFORMANCE REPORT HERE
        </div>
    </div>
</div>