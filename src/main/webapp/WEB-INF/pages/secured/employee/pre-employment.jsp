<%-- 
    Document   : pre-employment
    Created on : Dec 20, 2014, 5:52:52 PM
    Author     : Patrick Tan
--%>

<div class="panel panel-primary" ng-controller="manageRequirementsCtrl">
    <input type="hidden" ng-model="counter.reqCtr" ng-init="counter.reqCtr = 0" />
    <input type="hidden" ng-model="counter.empReqCtr" ng-init="counter.empReqCtr = 0" />

    <div class="panel-heading" role="tab" id="preEmploymentHeading">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#preEmployment" aria-expanded="false" aria-controls="preEmployment" ng-click="initRequirements()">
            PRE-EMPLOYMENT REQUIREMENTS 
            <span class="hidden">
                ({{counter.empReqCtr}}/{{counter.reqCtr}})
            </span>
        </a>
    </div>
    <div id="preEmployment" class="panel-collapse collapse" role="tabpanel" aria-labelledby="preEmploymentHeading">
        <div class="panel-body">   
            <div style="max-height: 406px; overflow-x: auto">
                <p style="margin-bottom: 0px" class="ng-hide" ng-show="showLoading">
                    Loading requirements information <img src="${baseUrl}/resources/img/loader.gif" />
                </p> 

                <ul class="summary">
                    <li data-toggle="tooltip" data-placement="bottom" title="Click to edit or view."
                        class="list-group-item" 
                        ng-repeat="requirement in items"
                        repeat-complete="doSomething(requirement)">

                        <div class="row" ng-show="requirement.employeeRequirements[0] &&
                                                    !requirement.employeeRequirements[0].notApplicable">
                            <div class="col-xs-7 col-sm-7 col-md-8 col-lg-7">
                                <h5>
                                    {{requirement.description}}
                                </h5>
                                <label>Received By</label>
                                <span>{{requirement.employeeRequirements[0].receivedBy}}</span>

                                <div ng-show="requirement.employeeRequirements[0].attachmentType" style="margin-top: 5px;">
                                    <button class="btn btn-xs btn-default" ng-click="update(requirement)">
                                        <span class="glyphicon glyphicon-floppy-open"></span> UPDATE
                                    </button>
                                    <button class="btn btn-xs btn-default" 
                                            ng-click="viewAttachment(requirement.employeeRequirements[0])">
                                        <span class="glyphicon glyphicon-paperclip"></span> VIEW ATTACHMENT
                                    </button>
                                </div>
                            </div>
                            <div class="col-xs-5 col-sm-5 col-md-4 col-lg-5">
                                <label>Submitted Date</label>
                                <span>
                                    {{requirement.employeeRequirements[0].dateSubmitted}}
                                </span>
                                <p class="list-group-item-text">
                                    {{requirement.employeeRequirements[0].remarks}}
                                </p>
                            </div>
                        </div>
                        <div class="row ng-hide" 
                             ng-show="!requirement.employeeRequirements
                                                         || requirement.employeeRequirements[0].notApplicable" 
                             >
                            <div class="col-sm-12">
                                <button class="btn btn-xs btn-default" ng-click="update(requirement)">
                                    <span class="glyphicon glyphicon-floppy-open"></span> UPDATE
                                </button>
                                <label>
                                    {{requirement.description}} <span ng-show="requirement.employeeRequirements[0].notApplicable">(NA)</span>
                                </label>
                                
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div id="viewRequirementAttachmentModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="row" style="padding: 10px">
                        <div class="alert alert-success ng-hide" ng-show="showAttachmentLoader" style="width: 97%;
                             margin: auto;
                             padding: 5px 10px;">
                            <p style="margin-bottom: 0px; font-size: 13px" class="text-success">
                                Please wait, retrieving attachment <img src="${baseUrl}/resources/img/loader.gif" />
                            </p>
                        </div>
                        <div class="col-lg-12 ng-hide attachment-container" ng-show="pdf" >
                            <object data="{{pdf}}" type="application/pdf"></object>
                        </div>
                        <div class="col-lg-12 ng-hide attachment-container" ng-show="image">
                            <img class="img-thumbnail" 
                                 style="height: 100%; width: 100%"                              
                                 src="{{image}}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- start modal -->
        <div id="employeeRequirementModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title text-primary">PRE-EMPLOYMENT REQUIREMENT {{employeeRequirement.notApplicable}}</h5>
                    </div>
                    <form method="post" enctype="multipart/form-data" ng-submit="upload()" novalidate>
                        <div class="modal-body">
                            <div class="row">
                                <input type="hidden"
                                       ng-model="employeeRequirement.primaryId"                                         
                                       />
                                <input type="hidden"
                                       ng-model="employeeRequirement.lookupRequirement.requirementId"                                         
                                       />

                                <div class="col-sm-5 col-md-5 col-lg-5">
                                    <div class="form-group">
                                        <label for="">Requirement:</label>
                                        <div class="input-group-sm">
                                            <span>{{requirement.description}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Not Applicable?</label>
                                        <div class="input-group-sm">
                                            <input type="checkbox"                              
                                                   ng-model="employeeRequirement.notApplicable" />
                                        </div>
                                    </div>
                                    <hr ng-hide="employeeRequirement.notApplicable">
                                    <div class="form-group" ng-hide="employeeRequirement.notApplicable">
                                        <label for="">Attachment:</label>
                                        <div class="input-group-sm">
                                            <input type="file" 
                                                   name="file" 
                                                   ng-model="file" 
                                                   data-rule-required="true" 
                                                   accept="image/*, application/msword, .docx, .pdf, .xls, .xlsx"
                                                   id="file">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-7 col-md-7 col-lg-7" ng-hide="employeeRequirement.notApplicable">                                       
                                    <div class="form-group">
                                        <label for="">Received By:</label>
                                        <div class="input-group-sm">
                                            <input type="text" 
                                                   class="form-control"                                      
                                                   ng-model="employeeRequirement.receivedBy" 
                                                   required="true" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Date Submitted:</label>
                                        <div class="input-group-sm">
                                            <input type="text" 
                                                   class="form-control datepicker"   
                                                   placeholder="YYYY-MM-DD"
                                                   maxlength="10"
                                                   ng-model="employeeRequirement.dateSubmitted"   
                                                   required="true" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Remarks:</label>
                                        <div class="input-group-sm">
                                            <textarea class="form-control"
                                                      ng-model="employeeRequirement.remarks" >
                                            </textarea>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">CLOSE</button>
                            <button type="subtmit" class="btn btn-sm btn-primary">SAVE CHANGES</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end modal -->
    </div>
</div>
