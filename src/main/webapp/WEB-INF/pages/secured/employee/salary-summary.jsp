<%-- 
    Document   : salary-summary
    Created on : Dec 20, 2014, 5:49:17 PM
    Author     : tanpa
--%>

<%@include file="../../common/taglibs.jsp" %>

<div class="panel panel-primary" ng-controller="manageSalariesCtrl">
    <div class="panel-heading" role="tab" id="salarySummaryHeading">
        <a data-toggle="collapse" data-parent="#accordion" href="#salarySummary" aria-expanded="true" aria-controls="salarySummary" ng-click="initSalaries()">
            <strong>SALARY SUMMARY</strong>
        </a>
    </div>
    <div id="salarySummary" class="panel-collapse collapse" role="tabpanel" aria-labelledby="salarySummaryHeading">
        <div class="panel-body">
            <div style="margin-left: 5px">                
                <button ng-click="manage()" class="btn btn-sm btn-success">
                    <span class="glyphicon glyphicon-edit"></span> NEW RECORD
                </button>
            </div>
            <hr>
            <div style="max-height: 406px; overflow-style: scrollbar; overflow: auto">
                <p style="margin-bottom: 0px" class="ng-hide" ng-show="showLoading">
                    Loading salary information <img src="${baseUrl}/resources/img/loader.gif" />
                </p> 

                <ul id="salarySummary" class="summary" ng-show="items.length > 0">
                    <li data-toggle="tooltip" data-placement="bottom" title="Click to edit or view."
                        class="list-group-item" 
                        ng-repeat="salary in items">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                <label>New Salary:</label>
                                <span>
                                    Php {{salary.newSalary}}
                                </span>
                                <br>
                                <label>Effective Date:</label>
                                <span>
                                    {{salary.effectiveDate}}
                                </span>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 bordered-left">
                                <label>Previous Salary:</label>
                                <span>
                                    Php {{salary.currentSalary}}
                                </span>
                                <br>
                                <label>Approval Date:</label>
                                <span>
                                    {{salary.adjustmentDate}}
                                </span>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-6 bordered-left">
                                <label>Adjustment Justification:</label>
                                <br>
                                <span>
                                    {{salary.adjustmentReason}}
                                </span>
                            </div>
                        </div>                        
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn btn-xs btn-default" ng-click="manage(salary)">
                                    <span class="glyphicon glyphicon-floppy-open"></span> EDIT
                                </button>
                            </div>
                        </div>
                    </li>
                </ul>
                <p style="margin-bottom: 0px" 
                   class="ng-hide" 
                   ng-show="items.length == 0">
                    No salary information available.
                </p>
            </div>
        </div>

        <!-- start modal -->
        <div id="salarySummaryModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h5 class="modal-title text-primary">SALARY SUMMARY</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <input type="hidden"                              
                                   ng-model="employeeSalary.encryptedKey"   
                                   />

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">
                                        New Salary <span class="required-field">*</span>
                                    </label>
                                    <div class="input-group-sm">
                                        <input type="text" 
                                               class="form-control"      
                                               ng-class="errors.newSalary ? 'validation-field' : ''"
                                               ng-model="employeeSalary.newSalary"
                                               required="true"
                                               maxlength="7" />

                                        <div class="text-right">
                                            <span ng-show="errors.newSalary" class="ng-hide validation-error">{{errors.newSalary}}</span>
                                        </div>
                                    </div>
                                </div>       
                                <div class="form-group">
                                    <label for="">
                                        Approval Date <span class="required-field">*</span>
                                    </label>
                                    <div class="input-group-sm">
                                        <input type="text" 
                                               class="form-control datepicker"            
                                               placeholder="YYYY-MM-DD"
                                               maxlength="10"
                                               ng-class="errors.adjustmentDate ? 'validation-field' : ''"
                                               ng-model="employeeSalary.adjustmentDate"      
                                               required="true" />

                                        <div class="text-right">
                                            <span ng-show="errors.adjustmentDate" class="ng-hide validation-error">{{errors.adjustmentDate}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        Effectivity Date <span class="required-field">*</span>
                                    </label>
                                    <div class="input-group-sm">
                                        <input type="text" 
                                               class="form-control datepicker"
                                               placeholder="YYYY-MM-DD"
                                               ng-class="errors.effectiveDate ? 'validation-field' : ''"
                                               ng-model="employeeSalary.effectiveDate"   
                                               maxlength="10"
                                               required="true" />

                                        <div class="text-right">
                                            <span ng-show="errors.effectiveDate" class="ng-hide validation-error">{{errors.effectiveDate}}</span>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">
                                        Previous Salary <span class="required-field">*</span>
                                    </label>
                                    <div class="input-group-sm">
                                        <input type="text" 
                                               class="form-control"          
                                               ng-class="errors.currentSalary ? 'validation-field' : ''"
                                               ng-model="employeeSalary.currentSalary" 
                                               required="true"
                                               maxlength="7" />

                                        <div class="text-right">
                                            <span ng-show="errors.currentSalary" class="ng-hide validation-error">{{errors.currentSalary}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        Adjustment Justification <span class="required-field">*</span>
                                    </label>
                                    <div class="input-group-sm">
                                        <textarea class="form-control"
                                                  ng-class="errors.adjustmentReason ? 'validation-field' : ''"
                                                  ng-model="employeeSalary.adjustmentReason" >
                                        </textarea>

                                        <div class="text-right">
                                            <span ng-show="errors.adjustmentReason" class="ng-hide validation-error">{{errors.adjustmentReason}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">                    
                            <div class="col-xs-12 text-left">
                                <span class="required-field">Fields marked with * are required.</span>
                            </div>
                        </div>
                    </div>                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">CLOSE</button>
                        <button type="button" class="btn btn-sm btn-primary" ng-click="saveSalary()">SAVE CHANGES</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
    </div>
</div>
