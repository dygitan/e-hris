<%-- 
    Document   : view-all
    Created on : Dec 20, 2014, 12:23:45 PM
    Author     : tanpa
--%>
<%@include file="../../common/taglibs.jsp" %>

<style>
    @media(max-width: 785px) {
        .boom {
            max-height: 70vh;
        }
    }
</style>

<div class="row" ng-app="employeeModule" ng-controller="viewEmployeeCtrl">
    <div class="col-sm-4 col-md-3 col-lg-2">
        <div class="panel panel-info text-left">
            <div class="panel-heading text-center">
                <span class="glyphicon glyphicon-filter"></span> <b style="font-size: 13px">FILTER EMPLOYEES</b>
            </div>
            <div class="panel-body text-left">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>
                                Department:
                            </label>
                            <div class="input-group-sm">
                                <select class="form-control"
                                        ng-change="filterPositions()"
                                        ng-model="departmentId">
                                    <option value="">-- ALL --</option>
                                    <c:forEach var="department" items="${requestScope.departments}">
                                        <option value="${department.departmentId}">${department.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>
                                Position:
                            </label>
                            <div class="input-group-sm">
                                <select class="form-control"
                                        ng-model="positionId">
                                    <option value="">-- ALL --</option>
                                    <option ng-repeat="position in positions" ng-value="position.positionId">
                                        {{position.name}}
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>
                                Search:
                            </label>
                            <div class="input-group-sm">
                                <input type="text" class="form-control"
                                       ng-model="queryText"/>
                            </div>
                        </div>
                    </div>                    
                </div>  
                <div class="row">
                    <div class="col-sm-12">
                        <hr>
                        <div class="text-right">
                            <button class="btn btn-sm btn-success" ng-click="filter()">
                                <span class="glyphicon glyphicon-search"></span> SEARCH
                            </button> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-8 col-md-9 col-lg-6" style="height:90vh; overflow: auto">
        <div class="alert alert-warning ng-hide" ng-show="!showLoading && (!items.length || items.length === 0)">
            <p style="margin-bottom: 0px; font-size: 13px">
                <span class="glyphicon glyphicon-exclamation-sign"></span> No record/s found.
            </p>
        </div>

        <div class="alert alert-success" ng-show="showLoading" style="background-color: white">
            <p style="margin-bottom: 0px; font-size: 13px" class="text-success">
                Please wait, retrieving records <img src="${baseUrl}/resources/img/loader.gif" />
            </p>
        </div>

        <div class="boom">
            <div class="list-group ng-hide" ng-show="items.length > 0">
                <a href="${baseUrl}/secured/employee/view/{{employee.encryptedKey}}" class="list-group-item" ng-repeat="employee in items">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-2 text-center">
                            <img src="../../../resources/img/profile.jpg" class="img-circle" alt="Responsive image" style="height: 110px; width: 110px; padding-bottom: 10px" />
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-10">
                            <h4 class="list-group-item-heading">
                                {{employee.lastName}}, {{employee.firstName}} {{employee.middleName}}
                            </h4>
                            <div style="margin-left: 5px">
                                <label style="font-size: 13px">
                                    {{employee.lookupPosition.name}} - {{employee.lookupPosition.lookupDepartment.name}}
                                </label>
                                <br>
                                <span class="glyphicon glyphicon glyphicon-calendar"></span> <span>{{employee.birthDate}}</span>
                                <br>
                                <span class="glyphicon glyphicon-envelope"></span> <span>{{employee.email}}</span>
                                <br>
                                <span class="glyphicon glyphicon-phone"></span> <span>{{employee.mobile}}</span>
                                <br>
                                <!--<span class="glyphicon glyphicon glyphicon-list hidden"></span> <strong class="hidden">Requirements</strong> 1/10-->
                                <br>
                                <span class="hidden">Disciplinary Notices (past 3 months) 1/10</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<script src="${baseUrl}/resources/js/app/dataservice.js"></script>
<script src="${baseUrl}/resources/js/app/employee.js"></script>
