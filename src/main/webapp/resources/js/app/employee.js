var app = angular.module('employeeModule', [
    'employeeCtrl',
    'dataService'
]);

var employeeCtrl = angular.module('employeeCtrl', []);

employeeCtrl.directive('repeatComplete', function() {
//    return function($scope, element, attrs) {        
    return function($scope, $element) {
        var notApplicable = false;

        if ($scope.requirement.employeeRequirements) {
            notApplicable = Boolean($scope.requirement.employeeRequirements[0].notApplicable);

            if (notApplicable) {
                $element.addClass('not-applicable');
            } else {
                $scope.counter.empReqCtr += 1;
            }
        } else {
            $scope.counter.reqCtr += 1;
            $element.addClass('list-group-item-warning');
        }

//        if ($scope.$last) {
//        }
    };
});

employeeCtrl.controller('viewEmployeeCtrl', [
    '$scope',
    '$http',
    'dataService',
    function($scope, $http, dataService) {
        $scope.showLoading = true;
        $scope.module = 'employee';
        dataService.query($scope);

        $scope.filter = function() {
            $scope.items = {};
            $scope.showLoading = true;
            $scope.params = {
                q: $scope.queryText,
                d: $scope.departmentId,
                p: $scope.positionId
            };
            $scope.url = '/secured/employee/json/all';
            dataService.query($scope);
        };

        $scope.filterPositions = function() {
            $http({
                method: 'GET',
                url: '/secured/lookup/json/positions/' +
                        $scope.departmentId
            }).success(function(response) {
                $scope.positions = response;
            });
        };
    }
]);

employeeCtrl.controller('manageLegalCtrl', [
    '$scope',
    '$http',
    'dataService',
    function($scope, $http, dataService) {
        $scope.addLegal = function() {
            $scope.legal = {};
            $('#employeeLegalModal').modal('show');
        };

        $scope.initLegal = function() {
            if (!$scope.items) {
                var encryptedKey = $scope.employee.encryptedKey;

                if (!encryptedKey) {
                    encryptedKey = 0;
                }

                $scope.showLoading = true;
                $scope.url = '/secured/employee/json/legal/' + encryptedKey;
                dataService.query($scope);
            }
        };

        $scope.upload = function() {
            var formData = new FormData();

            if (!$scope.legal) {
                $scope.legal = {};
            }

            if (!$scope.legal.employee) {
                $scope.legal.employee = {
                    encryptedKey: $scope.employee.encryptedKey
                };
            }

            formData.append('attachment', legalFile.files[0]);
            formData.append('legal', JSON.stringify($scope.legal));

            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            $http({
                method: 'POST',
                url: '/secured/upload/employee/legal',
                headers: {'Content-Type': undefined},
                data: formData,
                blockUI: true,
                transformRequest: function(data) {
                    return data;
                }
            }).success(function(response, status) {
                $scope.items = response.data.items;
                $('#employeeLegalModal').modal('hide');
            });
        };

        $scope.viewAttachment = function(legal) {
            $scope.showAttachmentLoader = true;

            $('#viewLegalAttachmentModal').find('.attachment-container').css('height', ($(window).height() * .6) + 'px');
            $('#viewLegalAttachmentModal').modal('show');

            $scope.legal = {
                encryptedKey: legal.encryptedKey
            };

            $scope.pdf = null;
            $scope.image = null;

            if (legal.attachmentType.indexOf('image/') !== -1) {
                $http.get('/secured/employee/legal/attachment/image/' + legal.encryptedKey)
                        .then(function(response) {
                            $scope.image = 'data:image/png;base64,' + response.data;
                            $scope.showAttachmentLoader = false;

                        });
            } else if (legal.attachmentType === 'application/pdf') {
                $scope.pdf = baseUrl + '/secured/employee/legal/attachment/pdf/' + legal.encryptedKey;
                $scope.showAttachmentLoader = false;
            }
        };
    }
]);

employeeCtrl.controller('manageMovementCtrl', [
    '$scope',
    '$http',
    'dataService',
    function($scope, $http, dataService) {
        $scope.filterPositions = function() {
            $http({
                method: 'GET',
                url: '/secured/lookup/json/positions/' +
                        $scope.employeeMovement.newLookupPosition.lookupDepartment.departmentId
            }).success(function(response) {
                $scope.positions = response;
            });
        };

        $scope.initMovements = function() {
            if (!$scope.items) {
                $scope.showLoading = true;
                $scope.url = '/secured/employee/json/movements/' + $scope.employee.encryptedKey;
                dataService.query($scope);
            }
        };

        $scope.manage = function(movement) {
            $scope.errors = {};
            $scope.employeeMovement = {};

            if (movement) {
                $http({
                    method: 'GET',
                    async: false,
                    url: '/secured/lookup/json/positions/' +
                            movement.newLookupPosition.lookupDepartment.departmentId
                }).success(function(response) {
                    $scope.positions = response;
                    $scope.employeeMovement = angular.copy(movement);
                    $('#movementModal').modal('show');
                });
            } else {
                $('#movementModal').modal('show');
            }
        };

        $scope.saveMovement = function() {
            if (!$scope.employeeMovement) {
                $scope.employeeMovement = {};
            }

            if (!$scope.employeeMovement.employee) {
                $scope.employeeMovement.employee = {
                    employeeId: $scope.employee.employeeId
                };
            }

            if ($scope.employeeMovement.newLookupPosition) {
                $scope.employeeMovement.employee.lookupPosition.positionId =
                        $scope.employeeMovement.newLookupPosition.positionId;
            }

            if (!$scope.employeeMovement.currentLookupPosition) {
                $scope.employeeMovement.currentLookupPosition = {
                    positionId: $scope.employee.lookupPosition.positionId
                };
            }

            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            var request = $http.post('/secured/employee/save/movement',
                    JSON.stringify($scope.employeeMovement),
                    {blockUI: true});

            request.success(function(response) {
                if (!response.errors) {
                    $scope.employee.lookupPosition.positionId =
                            $scope.employeeMovement.newLookupPosition.positionId;
                    $scope.items = response.data.movements;
                    $('#movementModal').modal('hide');
                } else {
                    $scope.errors = response.errors;
                }
            });
        };
    }
]);

employeeCtrl.controller('manageNotice', [
    '$scope',
    '$http',
    'dataService',
    function($scope, $http, dataService) {
        $scope.addNotice = function() {
            $('#noticeModal').modal('show');
        };

        $scope.saveNotice = function() {
            console.log($scope.employeeIr);
        };
    }
]);

employeeCtrl.controller('manageRequirementsCtrl', [
    '$scope',
    '$http',
    'dataService',
    function($scope, $http, dataService) {
        $('#employeeRequirementModal').on('hidden.bs.modal', function(e) {
            $scope.showLoading = false;
        });

        $scope.initRequirements = function() {
            if (!$scope.items) {
                var encryptedKey = $scope.employee.encryptedKey;

                if (!encryptedKey) {
                    encryptedKey = 0;
                }

                $scope.showLoading = true;
                $scope.url = '/secured/employee/json/requirements/' + encryptedKey;
                dataService.query($scope);
            }
        };

        $scope.update = function(requirement) {
            if (requirement.employeeRequirements && requirement.employeeRequirements.length > 0) {
                $scope.employeeRequirement = angular.copy(requirement.employeeRequirements[0]);
            } else {
                $scope.employeeRequirement = {
                    lookupRequirement: {
                        requirementId: requirement.requirementId
                    }
                };
            }

            $('#employeeRequirementModal').modal('show');
        };

        $scope.upload = function() {
            if (!$scope.employeeRequirement) {
                $scope.employeeRequirement = {};
            }

            if (!$scope.employeeRequirement.employee) {
                $scope.employeeRequirement.employee = {
                    encryptedKey: $scope.employee.encryptedKey
                };
            }

            var formData = new FormData();

            if (file.files && file.files.length > 0) {
                formData.append('attachment', file.files[0]);
            }

            formData.append('requirement', JSON.stringify($scope.employeeRequirement));

            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            $http({
                method: 'POST',
                url: '/secured/upload/employee/requirement',
                headers: {'Content-Type': undefined},
                data: formData,
                blockUI: true,
                transformRequest: function(data) {
                    return data;
                }
            }).success(function(response) {
                $scope.counter.reqCounter = 0;
                $scope.counter.empReqCounter = 0;
                $scope.items = response.data.items;
                $('#employeeRequirementModal').modal('hide');
            });
        };

        $scope.viewAttachment = function(requirement) {
            $scope.showAttachmentLoader = true;

            $('#viewRequirementAttachmentModal').find('.attachment-container').css('height', ($(window).height() * .6) + 'px');
            $('#viewRequirementAttachmentModal').modal('show');

            $scope.pdf = null;
            $scope.image = null;

            if (requirement.attachmentType.indexOf('image/') !== -1) {
                $http.get('/secured/employee/requirement/attachment/image/' + requirement.encryptedKey)
                        .then(function(response) {
                            $scope.image = 'data:image/png;base64,' + response.data;
                            $scope.showAttachmentLoader = false;

                        });
            } else if (requirement.attachmentType === 'application/pdf') {
                $scope.pdf = baseUrl + '/secured/employee/requirement/attachment/pdf/' + requirement.encryptedKey;
                $scope.showAttachmentLoader = false;
            }
        };
    }
]);

employeeCtrl.controller('manageSalariesCtrl', [
    '$scope',
    '$http',
    'dataService',
    function($scope, $http, dataService) {
        $scope.initSalaries = function() {
            if (!$scope.items && $scope.employee.encryptedKey) {
                $scope.showLoading = true;
                $scope.url = '/secured/employee/json/salaries/' + $scope.employee.encryptedKey;
                dataService.query($scope);
            }
        };

        $scope.manage = function(salary) {
            $scope.errors = {};

            if (salary) {
                $scope.employeeSalary = angular.copy(salary);
            }

            $('#salarySummaryModal').modal('show');
        };

        $scope.saveSalary = function() {
            if (!$scope.employeeSalary) {
                $scope.employeeSalary = {};
            }

            if (!$scope.employeeSalary.employee) {
                $scope.employeeSalary.employee = {
                    encryptedKey: $scope.employee.encryptedKey
                };
            }

            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            var request = $http.post('/secured/employee/save/salary',
                    JSON.stringify($scope.employeeSalary),
                    {blockUI: true});

            request.success(function(response) {
                if (!response.errors) {
                    $scope.items = response.data.salaries;
                    $('#salarySummaryModal').modal('hide');
                } else {
                    $scope.errors = response.errors;
                }
            });
        };
    }
]);

employeeCtrl.controller('manageEmployeeCtrl', [
    '$scope',
    '$http',
    function($scope, $http) {
        $scope.doSomething = function() {
            console.log('do something here')
        };

        $scope.filterPositions = function() {
            $http({
                method: 'GET',
                url: '/secured/lookup/json/positions/' +
                        $scope.employee.lookupPosition.lookupDepartment.departmentId
            }).success(function(response) {
                $('select#managePositions option[value!=""]').remove();
                $('select#managePositions option[value=""]').attr('selected', true);
                $scope.managePositions = response;
            });
        };

        $scope.save = function() {
            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            var request = $http.post('/secured/employee/save',
                    JSON.stringify($scope.employee),
                    {blockUI: true});

            request.success(function(response, status) {
                if (isDebug) {
                    console.log('response status [' + status + ']');
                    console.log(response);
                }

                if (!response.errors) {
                    $scope.errors = {};

                    $scope.employee.encryptedKey = response.data.encryptedKey;
                    $scope.employee.createdBy = response.data.createdBy;
                    $scope.employee.createdDate = response.data.createdDate;
                } else {
                    $scope.errors = response.errors;
                }
            });
        }
        ;
    }
]);

function processErrors(errors) {
    var temp = {};

    if (isDebug) {
        console.log('process errors');
        console.log(errors);
    }

    for (var index in errors) {
        var error = errors[index];
        temp[error.field] = error.defaultMessage;

        if (isDebug) {
            console.log('field name [' + error.field + ' - ' + error.defaultMessage + ']');
        }
    }

    return temp;
}