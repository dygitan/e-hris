package e201.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.suite.e201.config.spring.core.SpringConfig;
import hr.suite.e201.model.employee.Employee;
import hr.suite.e201.service.EmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author tanpa
 */
@ContextConfiguration(classes = {SpringConfig.class})
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@EnableAspectJAutoProxy
public class UserServiceImplIT {

    @Autowired
    private EmployeeService employeeService;

    public UserServiceImplIT() {
    }

    /**
     * Test of loadUserByUsername method, of class UserServiceImpl.
     */
    @Test
    public void testLoadUserByUsername() {
        Employee employee = employeeService.findById(1);
        
        ObjectMapper o = new ObjectMapper();
        o.convertValue(employee, String.class);
    }

}
